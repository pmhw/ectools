<?php
// +----------------------------------------------------------------------
// | EC Tools 易开发框架 为快速开发而生 创新、大胆、引领
// +----------------------------------------------------------------------
// | 作者：dpp 
// +----------------------------------------------------------------------
// | QQ：32579135
// +----------------------------------------------------------------------
// | 官网：https://pmhapp.com
// +----------------------------------------------------------------------

$show_error_msg = true;
$with_route = true;
$zsmb = false;
$ect_log = true;
$ect_debug_mode = true;

return [
    // 应用地址
    'app_host'         => env('app.host', ''),
    // 应用的命名空间
    'app_namespace'    => '',
    // 是否启用路由
    'with_route'       => $with_route,
    // 默认应用
    'default_app'      => 'index',
    // 默认时区
    'default_timezone' => 'Asia/Shanghai',

    // 应用映射（自动多应用模式有效）
    'app_map'          => [],
    // 域名绑定（自动多应用模式有效）
    'domain_bind'      => [],
    // 禁止URL访问的应用列表（自动多应用模式有效）
    'deny_app_list'    => [],

    // 异常页面的模板文件
    'exception_tmpl'   => app()->getThinkPath() . 'tpl/exception.html',

    // 错误显示信息,非调试模式有效
    'error_message'    => '页面错误！请稍后再试～',
    // 显示错误信息
    'show_error_msg'   => $show_error_msg,
    // 助手面板
    'zsmb'             => $zsmb,
    // 操作日志
    'ect_log'         => $ect_log,
    // 调试模式
    'ect_debug_mode'  => $ect_debug_mode,
    
    'copyright' => 'Ec_tools 易框架',
    
    'html'      => '<h1>Ec tools 易框架</h1>',

];
