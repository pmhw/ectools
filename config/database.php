<?php
/**
 * 主数据库配置中心
 * 
**/
$datasss = [
    
    'mysql' => [
        // 数据库类型
        'type'            => env('database.type', 'mysql'),
        // 服务器地址
        'hostname'        => env('database.hostname', '127.0.0.1'),
        // 数据库名
        'database'        => env('database.database', ''),//修改为自己的数据库名
        // 用户名
        'username'        => env('database.username', ''),//修改为自己的用户名
        // 
        'password'        => env('database.password', ''),//修改为自己的密码
        // 端口
        'hostport'        => env('database.hostport', '3306'),
        // 数据库连接参数
        'params'          => [],
        // 数据库编码默认采用utf8
        'charset'         => env('database.charset', 'utf8'),
        // 数据库表前缀
        'prefix'          => env('database.prefix', 'ect_'),

        // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
        'deploy'          => 0,
        // 数据库读写是否分离 主从式有效
        'rw_separate'     => false,
        // 读写分离后 主服务器数量
        'master_num'      => 1,
        // 指定从服务器序号
        'slave_no'        => '',
        // 是否严格检查字段是否存在
        'fields_strict'   => true,
        // 是否需要断线重连
        'break_reconnect' => false,
        // 监听SQL
        'trigger_sql'     => env('app_debug', true),
        // 开启字段缓存
        'fields_cache'    => false,
    ]
    // 更多的数据库配置信息
];
    
    

//提取APP目录
$_file = ECT_APP_PATH;

$folderList = [];   //最终返回的数组
//扫描目录内的所有目录和文件并返回数组
$path = $_file;
$data = scandir($path);

foreach ($data as $value) {
    //判断如果不是文件夹则进入下一次循环
    if (!is_dir($path . "/" . $value)) {
        continue;
    }
    if ($value != '.' && $value != '..') {
        $folderList[] = $value;
    }
}

$app_list = $folderList;

$b = [];//临时变量
$c = [];//临时变量
$app = [];//app程序
foreach ($app_list as $value){
   $file = ECT_APP_PATH . $value;
   
   if(is_dir($file)){
       //scandir方法
      $arr = array();
      $data = scandir($file);
      foreach ($data as $v){
        if($v != '.' && $v != '..'){
          $arr[] = $v;
        }
      }
    }
    
    $b[$value] = $arr;
}    

//读取后缀
foreach ($b as $key => $val){
    if(!empty($b[$key])){
       $d = [];
       foreach ($b[$key] as $val){

            $retval = "";
            
            $pt = strrpos ( $val , "." );
            
            if ( $pt ) $retval = substr ( $val , $pt +1, strlen ( $val ) - $pt );


           $d[] = $retval;
       }
       
       $b[$key] = $d;
    }
}

//重构app
$app_fonfig = [];
$k = 0;
foreach ($b as $key => $val){
    if(!empty($b[$key]) && in_array("json",$val)){
        in_array("json",$val);
        $json_string = file_get_contents($_file . '/' . $key.'/config.json'); 
        $data = json_decode($json_string, true);
        $data['file_name'] = $key;
        $app[] = $key;
        $app_fonfig[$k] = $data;
        $k++;
    }
}   


$aaa = [];
foreach ($app_fonfig as $k){
    $bbb = include(ECT_APP_PATH . $k['file_name'] . '/Mysql.php');
    $aaa[$k['file_name']] = $bbb;
}

// $count = count($a);
$_data = array_merge_recursive($datasss,$aaa);



return [
    // 默认使用的数据库连接配置
    'default'         => env('database.driver', 'mysql'),

    // 自定义时间查询规则
    'time_query_rule' => [],

    // 自动写入时间戳字段
    // true为自动识别类型 false关闭
    // 字符串则明确指定时间字段类型 支持 int timestamp datetime date
    'auto_timestamp'  => true,

    // 时间字段取出后的默认时间格式
    'datetime_format' => 'Y-m-d H:i:s',

    // 时间字段配置 配置格式：create_time,update_time
    'datetime_field'  => '',

    // 数据库连接配置信息
    'connections'     => $_data
];
