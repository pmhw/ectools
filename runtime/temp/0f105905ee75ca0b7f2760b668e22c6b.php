<?php /*a:3:{s:81:"/www/wwwroot/e.kaiyuantong.cn/app/ectools_view/Admin/ect_admin/set/set_system.htm";i:1668016761;s:79:"/www/wwwroot/e.kaiyuantong.cn/app/ectools_view/Admin/ect_admin/extends/Base.htm";i:1668502636;s:80:"/www/wwwroot/e.kaiyuantong.cn/app/ectools_view/Admin/ect_admin/public/jquery.htm";i:1651666477;}*/ ?>
<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <title>系统配置</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/admin/lib/layui-v2.6.3/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/admin/css/public.css" media="all">

    <style>
    input{color:#76838f;}
    </style>
</head>
<body>
<div class="layuimini-container">
    <div class="layuimini-main">
        
    <style>
        sub{color:#DEDEDE;}
    </style>
    <blockquote class="layui-elem-quote layui-text">
        开启调试模式后记录调试信息，记录执行过程，模版修改即使生效，显示异常信息，开启后性能会有一定下降。
    </blockquote>
    <form class="layui-form" action="">
        <div class="layui-form-item">
            <label class="layui-form-label" onclick="ect_debug_mode()">调试模式</label>
            <div class="layui-input-block">
                <input type="checkbox" name="ect_debug_mode" lay-skin="switch" lay-filter="ect_debug_mode"  lay-text="ON|OFF" <?php echo $ect_debug_mode==true ? 'checked' : ''; ?>>
            </div>
            <sub>定位程序完整错误信息，正式上线程序后请关闭</sub>
        </div>  
        
        
        <div class="layui-form-item">
            <label class="layui-form-label" onclick="show_error_msg()">显示错误</label>
            <div class="layui-input-block">
                <input type="checkbox" name="show_error_msg" lay-skin="switch" lay-filter="show_error_msg"  lay-text="ON|OFF" <?php echo $show_error_msg==true ? 'checked' : ''; ?>>
            </div>
            <sub>显示程序错误信息</sub>
        </div>
        
                
        <div class="layui-form-item">
            <label class="layui-form-label" onclick="zsmb_()">助手面板</label>
            <div class="layui-input-block">
                <input type="checkbox" name="zsmb" lay-skin="switch" lay-filter="zsmb"  lay-text="ON|OFF" <?php echo $zsmb==true ? 'checked' : ''; ?>>
            </div>
            <sub>配置后请F5/或手动刷新浏览器</sub>
        </div>  
        
        <div class="layui-form-item">
            <label class="layui-form-label" onclick="ect_log()">操作日志</label>
            <div class="layui-input-block">
                <input type="checkbox" name="ect_log" lay-skin="switch" lay-filter="ect_log"  lay-text="ON|OFF" <?php echo $ect_log==true ? 'checked' : ''; ?>>
            </div>
            <sub>记录管理操作日志 （存入数据库）</sub>
        </div>   
        
        <div class="layui-form-item">
            <label class="layui-form-label" onclick="close_()">系统日志</label>
            <div class="layui-input-block">
                <input type="checkbox" name="close" lay-skin="switch" lay-filter="close"  lay-text="ON|OFF" <?php echo $close==true ? 'checked' : ''; ?>>
            </div>
            <sub>全局日志写入，系统的错误日志记录 （会占用内存）</sub>
        </div>        

        
    </form>

    </div>
</div>
<script src="https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="/static/admin/js/message.js"></script>
<script>
    function ectmsg(title,code){

            switch(code)
            {
                case 0:
                    $.message({
                        message:title,
                        type:'success'
                    });
                    break;
                case 1:
                    $.message({
                        message:title,
                        type:'error'
                    });
                    break;
                case 2:
                    $.message({
                        message:title,
                        type:'warning'
                    });
                    break;
                case 3:
                    $.message({
                        message:title,
                        type:'info'
                    });
                    break;
                default:
            		$.message({
            			type:'success',
            			message:'<div style="color:#333;font-weight:bold;font-size:16px;">用户信息保存成功<div><span style="color:lightgrey;font-size:small;">'+title+'</span>',
            			duration:3000,
            			center:true
            		})
            }
    }
</script>
<script src="/static/admin/lib/layui-v2.6.3/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->



    <script>
    layui.use(['form', 'layedit', 'laydate', 'element'], function () {
        var form = layui.form
            , layer = layui.layer
            , layedit = layui.layedit
            , laydate = layui.laydate
            , element = layui.element;

        //日期
        laydate.render({
            elem: '#date'
        });
        laydate.render({
            elem: '#date1'
        });

        //创建一个编辑器
        var editIndex = layedit.build('LAY_demo_editor');

        //自定义验证规则
        form.verify({
            title: function (value) {
                if (value.length < 5) {
                    return '标题至少得5个字符啊';
                }
            }
            , pass: [
                /^[\S]{6,12}$/
                , '密码必须6到12位，且不能出现空格'
            ]
            , content: function (value) {
                layedit.sync(editIndex);
            }
        });

        //监听show_error_msg开关
        form.on('switch(show_error_msg)', function (data) {
            var type = this.checked ? '0' : '1';
            $.get('/EctAdminApi/show_error_msg',{type:type},function(res){
                if(res.code>0){
                    layer.msg(res.msg);
                }else{
                    layer.msg(res.msg); 
                }
                
            },'json');
            
            
            layer.tips('使用前确保调试模式打开，web根目录文件 <b>.example.env</b> 内的 <b>APP_DEBUG = true</b>', data.othis)
        });
        
        //监听close开关
        form.on('switch(close)', function (data) {
            var type = this.checked ? '0' : '1';
            $.get('/EctAdminApi/close',{type:type},function(res){
                if(res.code>0){
                    layer.msg(res.msg);
                }else{
                    layer.msg(res.msg); 
                }
                
            },'json');
        });        
        
        //监听zsmb开关
        form.on('switch(zsmb)', function (data) {
            var type = this.checked ? '0' : '1';
            $.get('/EctAdminApi/zsmb',{type:type},function(res){
                if(res.code>0){
                    layer.msg(res.msg);
                }else{
                    layer.msg(res.msg);
                    layer.open({
                        type:1,
                        content:'<div style="padding: 20px 100px;">请点击顶部刷新按钮,查看效果</div>',
                        time:3000,
                    });
                    setTimeout(function(){window.location.reload()},1000);
                    // setTimeout(function(){window.location.reload(true)},600);
                    //element.render('nav');
                }
            },'json');
        }); 
        
        //监听ect_log开关
        form.on('switch(ect_log)', function (data) {
            var type = this.checked ? '0' : '1';
            $.get('/EctAdminApi/ect_log',{type:type},function(res){
                if(res.code>0){
                    layer.msg(res.msg);
                }else{
                    layer.msg(res.msg);
                    $(".layui-tab-item.layui-show").find("iframe")[0].contentWindow.location.reload();
                    
                    // setTimeout(function(){window.location.reload(true)},600);
                    //element.render('nav');
                }
            },'json');
        });  
        
        //监听ect_debug_mode开关
        form.on('switch(ect_debug_mode)', function (data) {
            var type = this.checked ? '0' : '1';
            $.get('/EctAdminApi/ect_debug_mode',{type:type},function(res){
                if(res.code>0){
                    layer.msg(res.msg);
                }else{
                    layer.msg(res.msg);
                    $(".layui-tab-item.layui-show").find("iframe")[0].contentWindow.location.reload();
                    
                    // setTimeout(function(){window.location.reload(true)},600);
                    //element.render('nav');
                }
            },'json');
        });  
        
        //监听提交
        form.on('submit(demo1)', function (data) {
            layer.alert(JSON.stringify(data.field), {
                title: '最终的提交信息'
            })
            return false;
        });

        //表单初始赋值
        form.val('example', {
            "username": "贤心" // "name": "value"
            , "password": "123456"
            , "interest": 1
            , "like[write]": true //复选框选中状态
            , "close": true //开关状态
            , "sex": "女"
            , "desc": "我爱 layui"
        })


    });
    </script>



</body>


<div id="zsmb" style="background-color:#3f3f3f;padding:20px 30px;width:180px;height:100px;color:white;display:none;">//请点击任意标题查看信息</div>
<script>
    const zsmb = '<?php echo htmlentities(config('app.zsmb')); ?>';
     layui.use(['form', 'layedit', 'laydate'], function () {
        var form = layui.form
            , layer = layui.layer
            , layedit = layui.layedit
            , laydate = layui.laydate;           
           if(zsmb){
                layer.open({
                    type:1
                    ,title:'助手面板'
                    ,offset:'rt'
                    ,id:'zs'
                    ,content: $('#zsmb')
                    ,shade:0
                    ,yes:function(){
                        layer.closeAll();
                    }
                    ,cancel:function(){
                        //销毁后回调
                    }
                })
            }
     })

    function show_error_msg(){
        $('#zsmb').html('控制器所在目录：app/controller/EctAdminApi.php <br> 对应方法名：show_error_msg() <br> 当前显示：显示错误');
    }
    
    function close_(){
        $('#zsmb').html('控制器所在目录：app/controller/EctAdminApi.php <br> 对应方法名：close() <br> 当前显示：系统日志 <br> <sub>不建议开启，用于开发模式下查看错误日志</sub>');
    }
    
    function zsmb_(){
        $('#zsmb').html('控制器所在目录：app/controller/EctAdminApi.php <br> 对应方法名：zsmb()'); 
    }
    
    function ZSMB(key,value){
        $('#zsmb').html('配置标识：' + key + '<br> 当前显示：' + value + '<br/><br/>' + '<?php echo htmlentities(config('app.copyright')); ?>'); 
    }

</script>
</html>