<?php /*a:3:{s:79:"/www/wwwroot/e.kaiyuantong.cn/app/ectools_view/Admin/ect_admin/app/ect_code.htm";i:1668763111;s:79:"/www/wwwroot/e.kaiyuantong.cn/app/ectools_view/Admin/ect_admin/extends/Base.htm";i:1668502636;s:80:"/www/wwwroot/e.kaiyuantong.cn/app/ectools_view/Admin/ect_admin/public/jquery.htm";i:1651666477;}*/ ?>
<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/admin/lib/layui-v2.6.3/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/admin/css/public.css" media="all">

    <style>
    input{color:#76838f;}
    </style>
</head>
<body>
<div class="layuimini-container">
    <div class="layuimini-main">
        
    <style>
        /*.layui-btn{background-color:#ffffff;}*/
        .layui-layout-admin .layui-header {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            background-color: #23262E;
        }
    </style>
    <div class="layui-layout layui-layout-admin">
        <div class="layui-header header">
            <div style="margin:10px 10px 10px 10px;">
                <button type="button" onclick="add_file()" class="layui-btn layui-btn-primary layui-border-white">
                  保存<i id="add_file" class="layui-icon layui-icon-radio layui-font-12"></i>
                </button>
                <button type="button" onclick="load_frame()" class="layui-btn layui-btn-primary layui-border-white">
                  刷新<i class="layui-icon layui-icon-refresh-3 layui-font-12"></i>
                </button>
                
                <button type="button" onclick="delete_file()" class="layui-btn layui-btn-primary layui-border-white" id="delete">
                  删除<i class="layui-icon layui-icon-website layui-font-12"></i>
                </button>
                
                <button type="button" onclick="jump_function()" class="layui-btn layui-btn-primary layui-border-white" id="jump_function">
                  访问接口<i class="layui-icon layui-icon-website layui-font-12"></i>
                </button>
            </div>
        </div>
        <!--无限极左侧菜单-->
        <div class="layui-side layui-bg-black layuimini-menu-left" id="menu_left">
            <!--<div class="" style="width:100%;height:;"><a style="margin:10px 20px;color:#ffffff;">新建</a><a style="margin:0 10px;color:#ffffff;">删除</a></div>-->
            <ul class="layui-nav layui-nav-tree layui-inline" lay-filter="demo" style="margin-right: 10px;" id="menu_li">
              <?php if(is_array($files) || $files instanceof \think\Collection || $files instanceof \think\Paginator): $i = 0; $__LIST__ = $files;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
              <li class="layui-nav-item" onclick="jump('<?php echo htmlentities($vo); ?>')" id="fileName"><a><?php echo htmlentities($vo); ?></a></li>
              <?php endforeach; endif; else: echo "" ;endif; ?>
            </ul>
        </div>

        <div class="layui-body" style="padding-bottom:0px;">
            <iframe id="code" src="/ect_code_page" width="100%" height="100%" frameborder="0"></iframe>
        </div>

    </div>

    </div>
</div>
<script src="https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="/static/admin/js/message.js"></script>
<script>
    function ectmsg(title,code){

            switch(code)
            {
                case 0:
                    $.message({
                        message:title,
                        type:'success'
                    });
                    break;
                case 1:
                    $.message({
                        message:title,
                        type:'error'
                    });
                    break;
                case 2:
                    $.message({
                        message:title,
                        type:'warning'
                    });
                    break;
                case 3:
                    $.message({
                        message:title,
                        type:'info'
                    });
                    break;
                default:
            		$.message({
            			type:'success',
            			message:'<div style="color:#333;font-weight:bold;font-size:16px;">用户信息保存成功<div><span style="color:lightgrey;font-size:small;">'+title+'</span>',
            			duration:3000,
            			center:true
            		})
            }
    }
</script>
<script src="/static/admin/lib/layui-v2.6.3/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->


<script src="/static/admin/js/lay-config.js?v=2.0.0" charset="utf-8"></script>



<script>


$('.layui-btn').attr('style','color:#ffffff');

$('#jump_function').attr('style','color:#ffffff');


var file_path = '<?php echo htmlentities((isset($file) && ($file !== '')?$file:"")); ?>';
var directory_name = '<?php echo htmlentities((isset($file_name) && ($file_name !== '')?$file_name:"")); ?>';
var file_name = '<?php echo htmlentities((isset($file_name) && ($file_name !== '')?$file_name:"")); ?>';

var type = '<?php echo htmlentities((isset($type) && ($type !== '')?$type:"")); ?>';
var menu = '';
function jump(file){
    var frame_value = document.getElementById('code').contentWindow.type
    if(frame_value == 0){
        layer.msg('您还未保存代码');
        return;
    }
    
    if(type == 3){
                
        var url = '/ect_code_page?file=' + file + '&file_path=' + file_path + '&file_name=' + file_name + '&type=' + type;
        $('#code').attr("src",url);  
    }else{
        
        var url = '/ect_code_page?file=' + file + '&file_path=' + file_path + '&file_name=' + file_name;
        $('#code').attr("src",url);  
    }
}

// $('#menu_left').mousedown(function(e){ 
// 　　alert(e.which) // 1 = 鼠标左键 left; 2 = 鼠标中键; 3 = 鼠标右键 
// 　　return false;//阻止链接跳转 
// })


$(document).ready(function(){
 
    $(document).click(function(){
       layer.close(menu);
    });
 
});

layui.use(['dropdown', 'util', 'layer', 'table'], function(){
  var dropdown = layui.dropdown
  ,util = layui.util
  ,layer = layui.layer
  ,table = layui.table
  ,$ = layui.jquery;
  
    /**$('#menu_left').contextmenu(function(event){
        x=event.clientX; //top
        y=event.clientY; //left
        //console.log(event);
        btn=event.button;
        //console.log(btn);
        if(btn==2){
            layer.close(menu);
            menu = layer.open({
                type:1
                ,tipsMore: true
                ,title:false
                ,closeBtn:0
                ,offset:[x,y]
                ,id:'zs'
                ,content: '<div class="layui-panel"><ul class="layui-menu" id="demo1" onclick="new_menu()"><li lay-options=""></li><li lay-options=""><div class="layui-menu-body-title"><a >新建文件</a></div></li><li lay-options=""></li><li lay-options=""><div class="layui-menu-body-title"><a >删除文件</a></div></li></ul></div>'
                ,shade:0
                ,yes:function(){
                   layer.closeAll();
                }
            })
            return false;
        }
    });**/
    
    
    
    var data = [{title: '新建文件夹',id: 'test'}]
    
     //右键菜单
      var inst = dropdown.render({
        elem: '#menu_left' 
        ,trigger: 'contextmenu' 
        ,isAllowSpread: false 
        ,style: 'width: 160px' 
        ,id: 'menus' 
        ,data: data
        ,click: function(obj, othis){
          //console.log(othis)
          if(obj.id === 'test'){
              
            //layer.msg('click');

            layer.open({
                title: '新建文件',
                type: 2,
                shade: 0.2,
                maxmin:true,
                resizing:true,
                shadeClose: false,
                shade: 0,
                area: ['40%', '40%'],
                content: '/new_file/file_name/' + file_name + '/type/' + '<?php echo htmlentities($type); ?>',
            });
            
            
          } else if(obj.id === 'print'){
            window.print();
          } else if(obj.id === 'reload'){
            location.reload();
          }
        }
      });

    

});              


function new_menu(){
    //console.log(1);
    $('.layui-nav-item').append('<li class="layui-nav-item" onclick="jump()"><a>wenjian</a></li>');
}

function add_file(){
    document.getElementById('code').contentWindow.add_file(); 
}

function load_frame(){
   document.getElementById('code').contentWindow.load_frame();  
}

function jump_function(){
    document.getElementById('code').contentWindow.jump_function();
}

function delete_file(){
    document.getElementById('code').contentWindow.delete_file();
}

function msg_(title,code){
    ectmsg(title,code);
}

function reloads(){
    window.location.reload();
}

function open_web(url){
     layer.open({
        //title: '<i class="layui-icon layui-icon-website layui-font-18"></i> 访问接口界面'  + ' 域名：' + url,
        title: '运行界面',
        type: 2,
        shade: 0.2,
        maxmin:true,
        resizing:true,
        shadeClose: true,
        area: ['40%', '60%'],
        content: url,
    }); 
}

// 更新界面
function prepend_menu_li(file_name = 1){
    // $('#fileName').before('<li class="layui-nav-item layui-this" onclick="jump(\''+file_name+'\')" id="fileName"><a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">'+file_name+'</font></font></a></li>')
    // 保存
    document.getElementById('code').contentWindow.public_add_file();
    reloads();
}

function reloadss(){
    location.reload();
}


//prepend_menu_li();
</script>
<!--<ul class="layui-nav layui-nav-tree layui-inline" lay-filter="demo" style="margin-right: 10px;" id="menu_li">-->

<!--    <li class="layui-nav-item layui-this" onclick="jump('Index.php')" id="fileName"><a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">索引.php</font></font></a></li>-->
    
<!--    <li class="layui-nav-item" onclick="jump('config.json')" id="fileName"><a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">配置文件</font></font></a></li>-->
    
<!--    <span class="layui-nav-bar" style="top: 22.5px; height: 0px; opacity: 0;"></span>-->
<!--</ul>-->



</body>


<div id="zsmb" style="background-color:#3f3f3f;padding:20px 30px;width:180px;height:100px;color:white;display:none;">//请点击任意标题查看信息</div>
<script>
    const zsmb = '<?php echo htmlentities(config('app.zsmb')); ?>';
     layui.use(['form', 'layedit', 'laydate'], function () {
        var form = layui.form
            , layer = layui.layer
            , layedit = layui.layedit
            , laydate = layui.laydate;           
           if(zsmb){
                layer.open({
                    type:1
                    ,title:'助手面板'
                    ,offset:'rt'
                    ,id:'zs'
                    ,content: $('#zsmb')
                    ,shade:0
                    ,yes:function(){
                        layer.closeAll();
                    }
                    ,cancel:function(){
                        //销毁后回调
                    }
                })
            }
     })

    function show_error_msg(){
        $('#zsmb').html('控制器所在目录：app/controller/EctAdminApi.php <br> 对应方法名：show_error_msg() <br> 当前显示：显示错误');
    }
    
    function close_(){
        $('#zsmb').html('控制器所在目录：app/controller/EctAdminApi.php <br> 对应方法名：close() <br> 当前显示：系统日志 <br> <sub>不建议开启，用于开发模式下查看错误日志</sub>');
    }
    
    function zsmb_(){
        $('#zsmb').html('控制器所在目录：app/controller/EctAdminApi.php <br> 对应方法名：zsmb()'); 
    }
    
    function ZSMB(key,value){
        $('#zsmb').html('配置标识：' + key + '<br> 当前显示：' + value + '<br/><br/>' + '<?php echo htmlentities(config('app.copyright')); ?>'); 
    }

</script>
</html>