<?php
// +----------------------------------------------------------------------
// | EC Tools 易开发框架 为快速开发而生 创新、大胆、引领
// +----------------------------------------------------------------------
// | 作者：dpp 
// +----------------------------------------------------------------------
// | wx:N79823 （备注否则不予通过）
// +----------------------------------------------------------------------
// | 官网：https://pmhapp.com
// +----------------------------------------------------------------------

// 后台路由


use think\facade\Route;

Route::rule('ect_admin','_ect_admin/index');// 后台入口
Route::rule('ect_admin_index','_ect_admin/duindex');// 后台首页
Route::rule('ceshi','_ect_admin/ceshi');
Route::get('Vcode','Vcode/index');// 验证码

Route::rule('ect_login','_ect_admin_login/index');// 后台登录地址

Route::rule('set_system','_ect_admin/set_system');// 系统配置
Route::rule('set_web','_ect_admin/set_web');// 网站配置
Route::rule('zsmb','_ect_admin/zsmb');// 面板助手
Route::rule('ect_log','_ect_admin/ect_log');// 网站日志
Route::rule('ect_log_page','_ect_admin/ect_log_page');// 网站日志详情页
Route::rule('ect_system_info','_ect_admin/ect_system_info');// 系统信息
Route::rule('ect_code','_ect_admin/ect_code');// 程序开发-控制器
Route::get('program','_ect_admin/program');// 程序开发
Route::get('ect_code_page','_ect_admin/ect_code_page');// 程序开发详情页
Route::get('ect_code_view','_ect_admin/ect_code_view');// 程序开发-视图
Route::get('ect_code_extend','_ect_admin/ect_code_extend');// 程序开发-插件
Route::get('new_app','_ect_admin/new_app');// 程序开发-新建程序
Route::get('apptype','_ect_admin/apptype');// 程序开发
Route::get('new_file','_ect_admin/new_file');// 程序开发-新建文件
Route::get('EctCode','_ect_admin/EctCode');// EctCode桌面端程序
Route::get('EctPlugin','_ect_admin/EctPlugin');//  插件列表
Route::get('EctPluginList','_ect_admin/EctPluginList');//  插件列表
Route::get('EctApp','_ect_admin/EctApp');//  插件更新接口


Route::get('token','_electron_api/token');