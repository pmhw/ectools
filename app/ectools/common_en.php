<?php
// +----------------------------------------------------------------------
// | EC Tools 易开发框架 为快速开发而生 创新、大胆、引领
// +----------------------------------------------------------------------
// | 作者：dpp 
// +----------------------------------------------------------------------
// | QQ：32579135
// +----------------------------------------------------------------------
// | 官网：https://pmhapp.com
// +----------------------------------------------------------------------

use think\facade\Config;
use think\facade\View;//视图类库
use think\facade\Request;

//英文方法转译


/**
 * 断是否是微信内置浏览器
 * 
 * 作者qq32579135
 * 官网：https://pmhapp.com
 * 
**/
function isWeixin() { 
  if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) { 
    return true; 
  } else {
    return false; 
  }
}



//视图层渲染变量
function let($key,$value){
    View::assign($key,$value);
}

/**
 * json_exit @返回json数据
 * 
 * 作者qq32579135
 * 官网：https://pmhapp.com
 * 
 * $code @状态码
 * $msg @返回结果
**/
function json_exit($code = 0,$msg = 'ok',$value = ''){
    exit(json_encode(['code'=>$code,'msg'=>$msg,'value'=>$value],JSON_UNESCAPED_UNICODE));
}

// 视图渲染

/**

nginx+thinkphp的系统，$request->param()获得的值一直为空数组，后来查看源码发现里面有用到一个content-type，然后想起我nginx的fastcgi里我将content_type这一项给注释掉了，然后开启了后就OK了
**/
function view($file = ''){
    
    $str = Request::pathinfo();

    if($str){
        $data = explode("/",$str);
        if($file == ''){
            return View::fetch($data[0] . '/' . $data[2]);
        }else{
            return View::fetch($data[0] . '/' . $file);
        }
    }else{
        return View::fetch(Request::controller());
    }
    
     //dump(Request::controller());
}