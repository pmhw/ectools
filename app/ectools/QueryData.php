<?php
// +----------------------------------------------------------------------
// | EC Tools 易开发框架 为快速开发而生 创新、大胆、引领
// +----------------------------------------------------------------------
// | 作者：dpp 
// +----------------------------------------------------------------------
// | QQ：32579135
// +----------------------------------------------------------------------
// | 官网：https://pmhapp.com
// +----------------------------------------------------------------------

// 应用公共文件
use think\facade\Db;
use think\facade\Request;

/**
 * 数据库相关操作
**/

function out_path(){
    $request = Request::instance();
   
    // 取出路径 ks/Index/index
    $route = explode( "/",($request->pathinfo()))[0];
    if(!$route){
      $route =  explode( "/",($request->controller()))[0]; 
    }

    return $route;
}


/**
 * 查询单个数据
 * 
 * $table @表名称
 * $where @条件
**/
function query_one(String $table = '',Array $where = []){
    
    if($table == ''){
        return 'query_one: Method needs to pass in the table name';
    }
    
    if($where == null){
      return 'query_one: Method requires passing in conditions';  
    }

    $route = out_path();
    
    $data = Db::connect($route)->table($table)->where($where)->find();
        
    // 获取URL地址中的后缀信息
    return $data; 
}

function 查询单(String $table = '',Array $where = []){

    if($table == ''){
        return '查询单: 方法需要传入表名';
    }
    
    if($where == null){
      return '查询单: 方法需要传入条件';  
    }

    $request = Request::instance();
    
    // 取出路径 ks/Index/index
    $route = explode( "/",($request->pathinfo()))[0];
    
    $data = Db::connect($route)->table($table)->where($where)->find();
        
    // 获取URL地址中的后缀信息
    return $data;  
}


/**
 * 查询数据集
 * 
 * $table @表名称
 * $where @条件
 * $order @排序
**/

function query_more(String $table = '',$where = '',$order = ''){
    
    if($table == ''){
        return 'query_one: Method needs to pass in the table name';
    }
    
    
    if(empty($where)){
        $data = Db::connect(out_path())->table($table)->order($order)->select();
        
        return $data; 
        exit();
    }
    

    if(is_array($where)){
        $data = Db::connect(out_path())->table($table)->where($where)->order($order)->select();
    }else{
        $where = explode(",",$where);
        $data = Db::connect(out_path())->table($table)->where($where[0],$where[1],$where[2])->order($order)->select();
    }
    
    
        
    // 获取URL地址中的后缀信息
    return $data; 
}


function 查询多(String $table = '',$where = ''){
    
    if($table == ''){
        return '查询多: 方法需要传入表名';
    }
    
    if(is_array($where)){
        $data = Db::connect(out_path())->table($table)->where($where)->select();
    }else{
        $where = explode(",",$where);
        $data = Db::connect(out_path())->table($table)->where($where[0],$where[1],$where[2])->select();
    }
        
    // 获取URL地址中的后缀信息
    return $data; 
}


// 更新数据
function ect_update(String $table = '',Array $where = []){
    
    if($table == ''){
        return 'ect_update: The method needs to pass in the table name';
    }
    
    $svae = Db::name($table)
    ->save($where);
    
    if($svae){
        return true;
    }else{
        return false;
    }
}

/**
 * 插入数据
 * 
**/
function ect_insert(String $table = '',Array $data = []){
    
    if($table == ''){
        return 'ect_insert: The method needs to pass in the table name';
    }   
    
    $route = out_path();
    
    $insert = Db::connect($route)->table($table)->insert($data);
    
    if($insert){
        return true;
    }else{
        return false;
    }
}


/**
 * ect_data 接受参数
 * $type @ 数据传递类型 post get
 * $name @ 数据字段名
**/
function ect_data(String $type = '',String $name = ''){
    
    if($type == ''){
        return 'request method is empty get or post';
    }   
    
    return input($type . "." . $name);
}