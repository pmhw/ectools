<?php
namespace app\ectools_app;

// +----------------------------------------------------------------------
// | EC Tools 易开发框架 为快速开发而生 创新、大胆、引领
// +----------------------------------------------------------------------
// | 作者：dpp 
// +----------------------------------------------------------------------
// | 微信：N79823
// +----------------------------------------------------------------------
// | 官网：https://pmhapp.com
// +----------------------------------------------------------------------

use app\BaseController;
use think\facade\Db;
use think\Response;
use think\facade\View;
use think\facade\Session;


class EctAdminLogin{
    
    // 登陆界面
    public function index(){
        
        if(Session::get('ect_admin')){
            header('Location: /ect_admin');
        }
        
       return View::fetch("Admin/ect_admin_login/index");
        
    }
    
    // 管理员登录接口
    public function login(){
        $name = input('post.name');
        $password = input('post.password');
        $captcha = input('post.captcha');
        
        if(!$name || !$password || !$captcha){
            json_exit(1,'缺少参数');
        }
        
        if(!captcha_check($captcha)){
         // 验证失败
            json_exit(1,'请检查验证码');
        }
        
        $admin = Db::name('admin')->where(['name'=>$name])->find();
        
        if(!$admin){
            json_exit(1,'用户名和密码错误');  
        }
        
        if($admin['password'] != md5(md5($password))){
            json_exit(1,'用户名和密码错误'); 
        }
        
        Session::set('ect_admin',$admin);
        
        echo(json_encode([
            'code'=>0,
            'msg'=>'登陆成功',
            'value'=>''
        ]));
    }
    
    public function outlogin(){
        Session::delete('ect_admin');
        echo(json_encode([
            'code'=>0,
            'msg'=>'退出成功',
            'value'=>''
        ]));
    }
}