<?php
namespace app\ectools_app;

// +----------------------------------------------------------------------
// | EC Tools 易开发框架 为快速开发而生 创新、大胆、引领
// +----------------------------------------------------------------------
// | 作者：dpp 
// +----------------------------------------------------------------------
// | 微信：N79823
// +----------------------------------------------------------------------
// | 官网：https://pmhapp.com
// +----------------------------------------------------------------------

use app\BaseController;
use think\facade\Db;
use think\Response;

/**
 * admin json处理 包含：导航栏，标题，logo等等
 * 
 * 微信：N79823
 * 官网：https://pmhapp.com
 * 
 * 参考：http://layuimini.99php.cn/docs/init/thinkphp.html
**/


class EctAdminJson extends Base
{
    public function init(){
        
        $home = new home;
        $logo = new logo;
        
        //$data = [];
        $homeInfo = $home->info();
        $logoInfo = $logo->info();
        $menuInfo = $this->getMenuList();
        
        $appmenu = getFolderList(ECT_APP_PATH);
        $aaa = [];
        $max = Db::name('menu')->count();//获取最大id
        $aaa = [];
        foreach ($appmenu as $k){
            ++$max;
            $file = file_get_contents(ECT_APP_PATH . '/' . $k . '/menu.json');
            $bbb = !is_json($file);
            //dump($bbb);
            if($bbb){
 
                $bbb = json_decode($file,true);
                if($bbb['type'] == 0){
                    $bbb['id'] = $max;
                    foreach ($bbb['child'] as $kk){
                        $kk['gid'] = $max;
                    }
                    
                    
                    array_push($menuInfo,$bbb);
                }
            }
            
        }
        
        $data = ['homeInfo'=>$homeInfo,'logoInfo' => $logoInfo,'menuInfo'=>$menuInfo];
        
        //dump($data);
        //$json = substr($json, 1, strlen($json) - 2);
  
        $json_data = json_encode($data,JSON_UNESCAPED_UNICODE);
        
        //dump($json_data);
        return json($data);
    }
    
    
    
    /**
     * 获取子级菜单列表
     * 
     * 微信：N79823
     * 官网：https://pmhapp.com
    **/
    private function getMenuList(){
        $menuList = Db::name('menu')
            ->field('id,gid,title,icon,href,target')
            ->where('status', 1)
            ->order('sort_id', 'desc')
            ->select();
        $menuList = $this->buildMenuChild(0, $menuList);
        return $menuList;
    }

    /**
     * 获取父级菜单处理
     * 
     * 微信：N79823
     * 官网：https://pmhapp.com
     * 
     * $gid 父id
     * $menuList 菜单列表
    **/
    private function buildMenuChild($gid, $menuList){
        $treeList = [];
        foreach ($menuList as $v) {
            if ($gid == $v['gid']) {
                $node = $v;
                $child = $this->buildMenuChild($v['id'], $menuList);
                if (!empty($child)) {
                    $node['child'] = $child;
                }
                //后续此处加上用户的权限判断
                $treeList[] = $node;
            }
        }
        return $treeList;
    }
    
    /**
     * 日志查询接口
     * 
     * 微信：N79823
     * 官网：https://pmhapp.com
    **/
    
    public function ect_log(){
        
        $data = $this->TableInput();
        //dump($data);
        if(isset($data['searchParams'])){
            // 执行搜索
            
        }else{
            // 正常输出
            $ect_log = Db::name('log')
            ->page($data['page'],$data['limit'])
            ->order('id desc')
            ->select();
            
            
            $data = [];
            foreach ($ect_log as $key => &$value){
                $place_zh = TaoBaoIpApi . $value['place'] . TaoBaoIpApi_accessKey;
                $place_zh = geturl($place_zh);
                $place_zh = $place_zh['data']['city'];
                $value['place_zh'] = $place_zh;
                //dump($value);
                array_push($data,$value);
            }
            
            
            
            //dump($data);
            //exit($place_zh);
        }
        
        // 转换格式
        $json = $this->TableJson($data,count($ect_log));
        
        return $json;
    }
    
     /**
     * 表单请求接——收input参数
     * 
     * 微信：N79823
     * 官网：https://pmhapp.com
     * 
     * $data['page'] @查询页数
     * $data['limit'] @查询每页数量
    **/  
    private function TableInput(){
        // 获取参数
        $data['page'] = input('get.page');
        $data['limit'] = input('get.limit');
        
        if($data['limit'] > 100){
            json_exit('1','数据过大');
        }
        // 解析搜索参数
        if(input('?get.searchParams')){
            $searchParams = input('get.searchParams');
            $searchParams = json_decode($searchParams,true);
            $data['searchParams'] = $searchParams;
        }
        
        return $data;

    }
    
    /**
     * 表单请求构——造json数组
     * 
     * 微信：N79823
     * 官网：https://pmhapp.com
     * 
    **/
    private function TableJson($data,$count = 1){
        //构造函数        
        $_data = [
              "code"  => 0,
              "count" => $count,
              "msg"   => "",
        ];
        
        $_data['data']=$data;
        
        return json($_data);
        
    }
    
}


class home
{
    public function info(){
       return [
           'title'=>'首页',
           'href'=>'ect_admin_index',
       ];
       
    }
}

class logo
{
    public function info(){
      return [
          'title'=>'Ec tools',
          'image'=>'/static/logo.png',
          'href'=>'/ect_admin',
      ];  
    }
}