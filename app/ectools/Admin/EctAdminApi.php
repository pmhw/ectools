<?php
namespace app\ectools_app;

// +----------------------------------------------------------------------
// | EC Tools 易开发框架 为快速开发而生 创新、大胆、引领
// +----------------------------------------------------------------------
// | 作者：dpp 
// +----------------------------------------------------------------------
// | 微信：N79823
// +----------------------------------------------------------------------
// | 官网：https://pmhapp.com
// +----------------------------------------------------------------------

use app\BaseController;
use think\facade\View;
use think\facade\Config;
use think\facade\Db;
use think\facade\Request;
use ectlog\Log;
use think\facade\Session;


/**
 * 后台操作API接口
 * 
 * 作者 微信：N79823
 * 官网：https://pmhapp.com
**/

class EctAdminApi
{
    
    public function __construct(){

        //日志写入
        Log::runlog();
        
        $request = request();
        $url = $request->pathinfo();

        // 验证权限
        $admin = Session::get('ect_admin');
        // dump($admin);
        // exit();

        
        if($admin['type'] > 0){
            json_exit(1,'演示站不能修改');
        }
        
    }
    /**
     * 打开关闭调试模式时显示错误信息
     * 
     * 作者 微信：N79823
     * 官网：https://pmhapp.com
     * 
     * $type @状态
     * $file @文件目录
    **/
    public function show_error_msg(){
        
        $type = input('get.type');
        $file = config_path().'app.php';
        Config::set(['show_error_msg'=>$type],'app');
        
        if($type == 0){
            set_chmod($file,'$show_error_msg = false','$show_error_msg = true');
        }else{
            set_chmod($file,'$show_error_msg = true','$show_error_msg = false');
        }
        
        json_exit(1,'配置成功');
    }
    
    /**
     * 打开关闭日志写入
     * 
     * 作者 微信：N79823
     * 官网：https://pmhapp.com
     * 
     * $type @状态
     * $file @文件目录
    **/ 
    public function close(){
        $type = input('get.type');
        $file = config_path().'log.php';
        Config::set(['close'=>$type],'log');
        
        if($type == 0){
            set_chmod($file,'$close = false','$close = true');
        }else{
            set_chmod($file,'$close = true','$close = false');
        }
        
        json_exit(1,'配置成功');     
    }
    
     /**
     * 打开关闭面板助手
     * 
     * 作者 微信：N79823
     * 官网：https://pmhapp.com
     * 
     * $type @状态
     * $file @文件目录
    **/  
    public function zsmb(){
        $type = input('get.type');
        $file = config_path().'app.php';
        Config::set(['zsmb'=>$type],'app');
        
        if($type == 0){
            set_chmod($file,'$zsmb = false','$zsmb = true');
        }else{
            set_chmod($file,'$zsmb = true','$zsmb = false');
        }
        
        json_exit(0,'配置成功');   
    }
    
    /**
     * 打开关闭操作日志
     * 
     * 作者 微信：N79823
     * 官网：https://pmhapp.com
     * 
     * $type @状态
     * $file @文件目录
    **/      
    public function ect_log(){

        $type = input('get.type');
        $file = config_path().'app.php';
        Config::set(['ect_log'=>$type],'app');
        
        if($type == 0){
            set_chmod($file,'$ect_log = false','$ect_log = true');
        }else{
            set_chmod($file,'$ect_log = true','$ect_log = false');
        }
        
        json_exit(0,'配置成功');         
    }
    
    /**
     * 打开关闭调试模式
     * 
     * 作者 微信：N79823
     * 官网：https://pmhapp.com
     * 
     * $type @状态
     * $file @文件目录
     * 
    **/      
    public function ect_debug_mode(){

        $type = input('get.type');
        $file = config_path().'app.php';
        Config::set(['ect_debug_mode'=>$type],'app');
        
        $filee = app()->getRootPath();
        
        if($type == 0){
            $files = '.env_dome';
            $filess = '.env';
            
            set_rename($files,$filess,$filee);
            
            set_chmod($file,'$ect_debug_mode = false','$ect_debug_mode = true');
            
        }else{
            
            $files = '.env';
            $filess = '.env_dome';
            
            set_rename($files,$filess,$filee);
            set_chmod($file,'$ect_debug_mode = true','$ect_debug_mode = false');
        }
        
        json_exit(0,'配置成功');         
    }
    
    
    /**
     * 网站配置接口
     * 
     * 作者 微信：N79823
     * 官网：https://pmhapp.com
     * 
     * $_data @json格式数组
     * $update @保存到数据库
    **/ 
    public function set_web(){
        
        $data['web_title'] = input('post.web_title');
        $data['web_logo'] = input('post.web_logo');
        $data['web_seo_key'] = input('post.web_seo_key');
        $data['web_seo_desc'] = input('post.web_seo_desc');
        $data['web_copyright'] = input('post.web_copyright');
        $data['web_icp'] = input('post.web_icp');
        
        $_data = json_encode($data);
        
        $update = Db::name('config')->where('key','web_config')->update(['value'=>$_data]);
        
        if(!$update){
           json_exit(1,'保存失败'); 
        }
        json_exit(0,'保存成功');
    }
    
    /**
     * 删除ect_logs文件
     * 
     * 作者 微信：N79823
     * 官网：https://pmhapp.com
     * 
     * $_data @json格式数组
     * $update @保存到数据库
    **/  
    public function delete_ect_log(){
        
        if(!(Request::isGet())){
            
            $data = input('post.');
            
            $data = json_decode($data['data'],true);
            
            $data = array_column($data,'id');
            
            $delete = Db::name('log')->delete($data);

        }else{
            
           $id = input('get.id'); 
           $delete = Db::name('log')->where(['id'=>$id])->delete();
           
        }
        
        if(!$delete){
            json_exit(1,'删除失败');
        }
        json_exit(0,'删除成功');
    }
    
    /**
     * 上传网站logo
     * 
     * 作者 微信：N79823
     * 官网：https://pmhapp.com
     * 
     * $type @状态
     * $file @文件目录
     * 
    **/   
    function upload_logo(){
        $files = request()->file();
        //json_exit(0,'上传成功',$files);
        try {
            validate(['file'=>['fileSize'=>5*1024*1024,'fileExt'=>'jpg,png,jpeg']])
            ->check(['file'=>$files]);
            foreach ($files as $file){
                $savename = \think\facade\Filesystem::disk('public')->putFile( 'logo', $file);
                json_exit(0,'上传成功','/storage/'.$savename);
            }
        } catch (\think\exception\ValidateException $e) {
             exit(json_encode(['code'=>1,'msg'=>$e->getMessage()]));
        }
    }
    
    /**
     * 保存代码
     * 
     * 作者 微信：N79823
     * 官网：https://pmhapp.com
     * 
    **/      
    
    public function add_file(){
      $data = input('post.data');  
      $file_path = input('post.file_path');
      
      $_data = file_get_contents($file_path);
      
      chmod($file_path,0777);
      //
      $insert = file_put_contents($file_path,$data,FILE_USE_INCLUDE_PATH);
      chmod($file_path,0644);
      if(!$insert){
          json_exit(1,'保存失败',$insert);
      }
      json_exit(0,'保存成功',$insert);
      
    }
    
    /**
     * 访问接口
     * 
     * 作者 微信：N79823
     * 官网：https://pmhapp.com
     * 
     * $function_name @方法名
     * $ectool_name @控制器名
     * $directory_name @目录名 
     * $url @route前缀
     * $url2 @route值
     * $data @插入文件内容
    **/       
    public function jump_function(){
        
        $function_name = input('get.function_name');
        $ectool_name = input('get.ectool_name');
        $directory_name = input('get.directory_name');
        
        
        //检查文件是否存在 不存在生成
        chmod(ROUTE_PATH,0777);
        if(!is_file(ROUTE_PATH . $directory_name . '.php')){
        is_file(ROUTE_PATH . $directory_name . '.php') OR fclose(fopen(ROUTE_PATH . $directory_name . '.php', 'w'));
        $dome = file_get_contents(ROUTE_PATH . 'dome');
        file_put_contents(ROUTE_PATH . $directory_name . '.php',$dome,FILE_USE_INCLUDE_PATH);
        }
        chmod(ROUTE_PATH,0755);
        
        
        $file = file_get_contents(ROUTE_PATH . $directory_name . '.php');
        
        $url = $directory_name.'/'.$ectool_name.'/'.$function_name;
        
        $url2 = '\app\ectools_app\\' . $directory_name . '\\' . $ectool_name . '@' . $function_name;
        
        //json_exit(0,'创建成功',$file);
        
        $data = $file . "\n" . "Route::rule('" . $url . "', '" . $url2 . "');";
        
        if(strpos($file,$url) > 0 ){
            //包含
            json_exit(0,'已创建',$_SERVER['HTTP_HOST'] . '/' . $url);
        }
        
        chmod(ROUTE_PATH . $directory_name . '.php',0777);
        $insert = file_put_contents(ROUTE_PATH . $directory_name . '.php',$data,FILE_USE_INCLUDE_PATH);
        chmod(ROUTE_PATH . $directory_name . '.php',0755);
        if($insert){
            json_exit(0,'创建成功',$_SERVER['HTTP_HOST'] . '/' . $url);
        }
    }
    
    
    /**
     * 新建程序接口
     * 
     * 作者 微信：N79823
     * 官网：https://pmhapp.com
     * 
     * $app_filename @入口文件名
     * $file @写入路径 控制器
     * $file_ @写入路径 视图层
     * $copy_index $copy_json $copy_route @读取文件路径
     * $data @对应文件内容
     * 
    **/
    public function new_app(){
        
        $app_filename = input('get.app_filename');
        
        if(!$app_filename){
            json_exit(1,'文件名为空');
        }
        
        if(!(preg_match ("/^[a-z]/i", $app_filename))){
            json_exit(1,'请输入纯字母');
        }
        
        $app_filename = ucfirst(trim($app_filename));
        
        $file = APP_PATH . 'ectools_app';
        $file_ = APP_PATH . 'ectools_view';
        
        // 取出数据
        $copy_index = APP_PATH . 'ectools/index';
        $copy_json = APP_PATH . 'ectools/json';
        $copy_route = APP_PATH . 'ectools/route';
        $copy_view = APP_PATH . 'ectools/view';
        $copy_mysql = APP_PATH . 'ectools/mysql';
        $copy_menu = APP_PATH . 'ectools/menu';
        
        if(is_dir($file.'/'.$app_filename) || is_dir($file_.'/'.$app_filename)){
           json_exit(1,'入口名已存在'); 
        }
        
        //创建目录
        mkdir ($file.'/'.$app_filename,0777,true);//控制器文件
        mkdir ($file_.'/'.$app_filename,0777,true);//视图层文件
        
        chmod($file,0777);
        chmod($file_,0777);

            //写入index控制器文件
            $data = file_get_contents($copy_index);
            is_file($file.'/'.$app_filename.'/Index.php') OR fclose(fopen($file.'/'.$app_filename.'/Index.php', 'w'));
            
            file_put_contents($file.'/'.$app_filename.'/Index.php',$data);
            set_chmod($file.'/'.$app_filename.'/Index.php','wx',$app_filename);
            //chmod($file.'/'.$app_filename.'/Index.php',0755);
    
            
            //写入json配置文件
            $data = file_get_contents($copy_json);
            is_file($file.'/'.$app_filename.'/config.json') OR fclose(fopen($file.'/'.$app_filename.'/config.json', 'w'));
            file_put_contents($file.'/'.$app_filename.'/config.json',$data);
            chmod($file.'/'.$app_filename.'/config.json',0755);
            
            //写入mysql文件
            $data = file_get_contents($copy_mysql);
            is_file($file.'/'.$app_filename.'/Mysql.php') OR fclose(fopen($file.'/'.$app_filename.'/Mysql.php', 'w'));
            file_put_contents($file.'/'.$app_filename.'/Mysql.php',$data);
            chmod($file.'/'.$app_filename.'/Mysql.php',0755);            
    
    
            //写入menu文件
            $data = file_get_contents($copy_menu);
            is_file($file.'/'.$app_filename.'/menu.json') OR fclose(fopen($file.'/'.$app_filename.'/menu.json', 'w'));
            file_put_contents($file.'/'.$app_filename.'/menu.json',$data);
            chmod($file.'/'.$app_filename.'/menu.json',0755);         
            
            //写入视图层
            $data = file_get_contents($copy_view);
            is_file($file_.'/'.$app_filename.'/view.htm') OR fclose(fopen($file_.'/'.$app_filename.'/view.htm', 'w'));
            file_put_contents($file_.'/'.$app_filename.'/view.htm',$data);
            chmod($file_.'/'.$app_filename.'/view.htm',0755);        
        
        chmod($file,0755);
        chmod($file_,0755);
        chmod($file.'/'.$app_filename,0755);
        
        
        //写入路由
        $data = file_get_contents($copy_route);
        chmod(ROUTE_PATH,0777);
        is_file(ROUTE_PATH . $app_filename . '.php') OR fclose(fopen(ROUTE_PATH . $app_filename . '.php', 'w'));
        file_put_contents(ROUTE_PATH . $app_filename . '.php',$data);
        chmod(ROUTE_PATH,0755);
        
        
        json_exit(0,'创建成功');
        
    }
    
        
    /**
     * 导入程序接口
     * 
     * 作者 微信：N79823
     * 官网：https://pmhapp.com
     * 
     * $file_name @目录名
     * 
    **/
    
    public function update_app(){
        
            header('Content-type:text/html;charset=utf-8');
                
            if(!empty($_FILES['file']['name'])){
        
        
                if($_FILES['file']['error'] == 0){  // 判断上传是否正确
                    $fileName = $_FILES['file']['name'];  // 获取文件名称
                    $fileSize = $_FILES['file']['size'];  // 获取文件大小
                    $tmp_name = $_FILES["file"]["tmp_name"]; // 获取上传文件默认临时地址
                    $fileTypeInfo = ['ect'];  // 定义允许上传文件类型【很多种只列举3种】
                    
                    $fileType = substr(strrchr($fileName,'.'),1); // 提取文件后缀名
                    
                    if(!in_array($fileType,$fileTypeInfo)){  // 判断该文件是否为允许上传的类型
                    
                        exit(json_encode(['code'=>1,'msg'=>'格式不正确']));// 显示错误信息
        
                    }
                    if($fileSize/1024  > 1024*100){  // 规定文件上传大小【文件为Byte/1024 转为 kb】
                        exit(json_encode(['code'=>1,'msg'=>'文件太大']));// 显示错误信息
                    }

                    if(!file_exists(public_path() . '/storage/linshi/')){  // 判断是否存在存放上传文件的目录
                        mkdir(public_path() . '/storage/linshi/');  // 建立新的目录
                    }
                    
                    $newFileName = 'ect_app_' . time() . '.zip';  // 命名新的文件名称
                     
                    move_uploaded_file($tmp_name, public_path() . '/storage/linshi/' . $newFileName);  // 移动文件到指定目录
                    

                    $zip = new \ZipArchive;
                    $zip->open(public_path() . '/storage/linshi/' . $newFileName, \ZipArchive::CREATE);
                    
                    // 提取配置文件
                    $filesInside = [];
                    for ($i = 0; $i < $zip->count(); $i++) {
                        if(strpos($zip->getNameIndex($i),'config.json') !== false){
                            array_push($filesInside, $zip->getNameIndex($i));
                        }
                    }
                    
                    
                    if($filesInside == [] || !$filesInside){
                        deldir(public_path() . '/storage/linshi/');
                        exit(json_encode(['code'=>1,'msg'=>'程序格式不正确','value'=>$filesInside]));
                    }
                    
                    $strsToArray = strsToArray($filesInside[0],"/");
                    
                    
                    $appname = $strsToArray[0];
                    
                    $getFolderList = getFolderList(APP_PATH."ectools_app");
                    
                    
                    if(in_array($appname,$getFolderList)){
                        deldir(public_path() . '/storage/linshi/');
                        exit(json_encode(['code'=>1,'msg'=>'检测到该程序已存在，请重命名你的程序后再上传',]));
                    }
           
                    $zip->extractTo(public_path() . '/storage/linshi/');
                    $zip->close();
                    // 移动程序
                    rename(public_path() . '/storage/linshi/' . $strsToArray[0] . '/ectools_app/' . $strsToArray[0],APP_PATH.'ectools_app/' . $strsToArray[0]);
                    rename(public_path() . '/storage/linshi/' . $strsToArray[0] . '/ectools_view/' . $strsToArray[0],APP_PATH.'ectools_view/' . $strsToArray[0]);
                    
                    deldir(public_path() . '/storage/linshi/');
                    
                    exit(json_encode(['code'=>0,'msg'=>'上传成功','value'=>public_path() . '/storage/linshi/' . $strsToArray[0] . '/ectools_app/' . $strsToArray[0]]));

                }else{
                    exit(json_encode(['code'=>1,'msg'=>'上传失败']));
                }
        
            }

    }
    
    /**
     * 新建文件接口
     * 
     * 作者 微信：N79823
     * 官网：https://pmhapp.com
     * 
     * $file_name @文件名
     * $file_names @目录名
     * $type @ 属性 1 控制器 2视图层
    **/    
    public function new_file(){
        $file_name = input('get.file_name');
        if(!$file_name){
            json_exit(1,'文件名为空');
        }        
        $file_names = input('get.file_names');
        if(!$file_names){
            json_exit(1,'路径名为空');
        }
        
        $type = input('get.type');
        if(!$type){
            json_exit(1,'type为空');
        }
        // 
        if($type == 1){
            
            $file =  RootPath() . 'app/ectools_app/' . $file_names;
            chmod($file,0777);
        }else{
            // 取后缀
            $_file_name = file_1($file_name);
            if($_file_name != 'htm'){
               json_exit(1,'视图层，后缀必须为.htm'); 
            }
            
            $file =  RootPath() . 'app/ectools_view/' . $file_names;  
            chmod($file,0777);
        } 
        
        fopen($file . '/' . $file_name, "w");
        chmod($file,0755);
        
      
        json_exit(0,'创建成功');
        
        
    }
    
    /**
     * 删除文件接口
     * 
     * 作者 微信：N79823
     * 官网：https://pmhapp.com
     * 
     * $file_path @文件路径
     * $file_names @目录名
     * $type @ 属性 1 控制器 2视图层
    **/     
    public function delete_file(){
        $file_path = input('get.file_path');
        if (file_exists($file_path)) {
          //文件存在
          chmod($file_path,0777);
            if (unlink($file_path)) {
              //删除成功
              json_exit(0,'删除成功');
            } else {
              //删除失败
              json_exit(1,'删除失败');
            }
          chmod($file_path,0755);
        } else {
          //文件不存在
          json_exit(1,'文件不存在');
        }
    }
    
    /**
     * 删除程序接口
     * 
     * 作者 微信：N79823
     * 官网：https://pmhapp.com
     * 
     * $file_name @目录名
     * 
    **/
    public function delete_app(){
        $file_name = input('get.file_name');
        
        if(!$file_name){
            json_exit(1,'文件名为空');
        }
        
        chmod(ECT_APP_PATH,0777);
        if(file_exists(ECT_APP_PATH . $file_name)){
            deldir(ECT_APP_PATH . $file_name);
        }
        chmod(APP_PATH . 'ectools_view',0777);
        if(file_exists(APP_PATH . 'ectools_view/' . $file_name)){
            deldir(APP_PATH . 'ectools_view/' . $file_name);
        }
        chmod(ROUTE_PATH,0777);
        if(file_exists(ROUTE_PATH . $file_name . '.php')){
            unlink(ROUTE_PATH . $file_name . '.php');
        }
        
        chmod(APP_PATH . 'ectools_view',0755);
        chmod(ECT_APP_PATH,0755);
        chmod(ROUTE_PATH,0755);
        
        
        $default_controller = Config::get('route.default_controller');
        $_default_controller = $file_name . '/index';
        
        if($default_controller == $_default_controller){
            reduction_default_controller();
        }
        
        json_exit(0,'删除成功');
    }
    
    /**
     * 导出程序接口
     * 
     * 作者 微信：N79823
     * 官网：https://pmhapp.com
     * 
     * 
     * $file_name @目录名
     * 
    **/    
    public function export_app(){
        
        $php_loaded_extensions = (system_info())['php_loaded_extensions'];
        
        if(!in_array('zip',$php_loaded_extensions)){
            json_exit(1,'请先安装zip扩展');
        }
        
        $name = input('file_name');
        if(!$name){
           json_exit(1,'请检测参数'); 
        }
        
        //判断目录是否存在
        if(!is_dir(public_path() . 'export')){
            mkdir(public_path() . 'export',0755,true);
        }
        
        
        // Using functions
        $filename = $name . '_' . time();//定义导出压缩包名称
        chmod(APP_PATH.'ectools_app/'.$name,0777);
        chmod(APP_PATH.'ectools_view/'.$name,0777);
        chmod(public_path() . 'export',0777);
            $datas = [APP_PATH.'ectools_app/'.$name.'/',APP_PATH.'ectools_view/'.$name.'/'];
            $a = dirToZip($datas,public_path() . 'export/' . $filename .'.zip',$name);
        chmod(APP_PATH.'ectools_app/'.$name,0755);
        chmod(APP_PATH.'ectools_view/'.$name,0755);
        chmod(public_path() . 'export',0755);
        
            
        set_rename($filename . '.zip',$filename . '.ect',public_path() . 'export/');
        
        $url = '/export/' . $filename . '.ect';

         
        if($a['code'] == -200){
           json_exit(0,$a['msg']);  
        }
        
        if($a['code'] == 200 ){
           json_exit(0,'导出成功',$url); 
        }
        
        
    }
    
    /**
     * 配置入口文件名
     * 
     * 作者 微信：N79823
     * 官网：https://pmhapp.com
     * 
     * $file_name 程序文件名目录名
    **/
    public function config_default_controller(){
        $file_name = input('get.file_name');
        
        if(!$file_name){
           json_exit(1,'请检测参数'); 
        }
        
        $file = config_path() . 'route.php';
        
        $default_controller = Config::get('route.default_controller');
        $_default_controller = $file_name . '/index';

        if($_default_controller == $default_controller){
            reduction_default_controller();
            json_exit(0,'已还原设置'); 
        }else{
            //exit('$default_controller = "' . $default_controller . '";');
            set_chmod($file,'$default_controller = "' . $default_controller . '";','$default_controller = "' . $_default_controller .'";');
            
        }
        
        json_exit(0,'设置成功'); 
    }
    
    /**
     * 修改配置信息- 密钥
     * 
     * 作者 微信：N79823
     * 官网：https://pmhapp.com
     * 
    **/
    
    public function load_ApiToken(){
        $token = getToken();
        
        if(!$token){
            json_exit(1,'失败');
        }
        
        if(Db::name('config')->where('key','ectToken')->find()){
            // 如果字段存在更新
            Db::name('config')->where('key','ectToken')->update(['value'=>$token]);
        }else{
            // 如果字段不存在 写入
            Db::name('config')->insert(['key'=>'ectToken','value'=>$token]);
        }
        json_exit(0,'成功');
    }
    
    /**
     * TODO::待修复 获取问价.php 文件名 去除php => 循环取出函数名 => 清楚route 路由 重新写入
     * 
     * 
     * 一键修复路由
     * 
     * $name @ect程序名
     * get_class_methods('app')
    **/
    
    public function xiufu_route(){
        
        $name = input('get.name');
        $file = ECT_APP_PATH . $name;

        $getDirContent = getDirContent($file);
        
        $no_route=[];
        // 提取文件
        $datas = [];
        if($getDirContent){
            foreach ($getDirContent as $key){
                if(file_1($key) == "php" && file_0($key) !== 'Mysql' && $key !== 'Base.php'){
                    $file_string = file_get_contents($file .'/' . $key);
                    //dump($file_string);
                    if(strpos($file_string,'__construct') || strpos($file_string,'Base')  || $file_string == ""){
                        array_push($no_route,$key);
                       //dump(1);
                    }else {
                        
                            eval("\$String = get_class_methods(new \app\\ectools_app\\". $name ."\\" . file_0($key) . "());");
                            if(array_search('__construct',$String) !== false){
                                array_splice($String,array_search('__construct',$String),1);
                            }
                            $datas[$key] = $String;
                     
                    }
                }
            }
        }
        
        // 重命名备份route
        
        $routeFile = ROUTE_PATH . $name . '.php';
        chmod(ROUTE_PATH,0777);
        if(file_exists($routeFile)){
            if(!rename($routeFile,ROUTE_PATH . $name . '.php1')){
                json_exit(1,'备份路由失败!');
            }
        }
        
        $copy_route = APP_PATH . 'ectools/route';
        //写入
        $data = file_get_contents($copy_route);
        is_file($routeFile) OR fclose(fopen($routeFile, 'w'));
        file_put_contents($routeFile,$data);   
       
        // chongxinduqu
        $data = file_get_contents($routeFile);
        $dt = "";
        foreach($datas as $key => $value){
            foreach ($value as $k){
                $url1 = $name.'/'.file_0($key).'/'.$k;
                $url2 = '\app\ectools_app\\' . $name . '\\' . file_0($key) . '@' . $k;
                $dt .= "\n" . "Route::rule('" . $url1 . "', '" . $url2 . "');";
            }
        }

        $insert = file_put_contents($routeFile,$data . $dt,FILE_USE_INCLUDE_PATH);
        if(!$insert){
            rename(ROUTE_PATH . $name . '.php1',ROUTE_PATH . $name . '.php');
        }else{
            unlink(ROUTE_PATH . $name . '.php1');
        }
        
        chmod(ROUTE_PATH,0755);
        if(!$insert){
            json_exit(1,'重制失败!');
        }
        $count = count($no_route);
        $i = 1;
        if($no_route !== []){
            $msg = "重制完成!<br>以下文件未重置：";
            foreach ($no_route as $k){
                 if($count == $i){
                   $msg .= "<br> <a style='color:red;'>({$i}) " . $k ."</a>";  
                 }else{
                     $msg .= "<br> <a style='color:red;'>({$i}) " . $k .",</a>";
                 }
                 $i++;

            }
            
            json_exit(0,$msg . "<br>可能因为: <br>1.extends 请先删除  <br> 2.存在__construct函数请先删  <br>3.该文件为空 ");
        }else{
        json_exit(0,'重制完成!');
        }

    }
    
    // 检测核心库版本
    public function ect_check_updates(){
        ect_check_updates();
    }
    
    // 更新核心库版本
    public function ect_updates(){
        ect_updates();
    }
    
    
    // 检测是否配置数据库()
    
    public function ect_mysql_admin(){
        
        $name = input('post.name');
        
        if(!$name){
            json_exit(1,'缺少参数',$name);
        }
        $file = app_path() . 'ectools_app/' . $name . '/Mysql.php';
        
        $data = include $file;
        
        
         // 从文件中读取数据到PHP变量  
        $json_string = file_get_contents(app_path() . 'ectools_app/' . $name . '/config.json');  
          
        // 用参数true把JSON字符串强制转成PHP数组  
        $json_data = json_decode($json_string, true);  
        
        if(!empty($json_data['mysql_url'])){
            header('Location:' . $data['mysql_url']);
        }
        
        if($data['username'] == '' || $data['password'] == '' || $data['database'] == ''){
            json_exit(1,'请检查 Mysql.php 文件配置');
        }
        
        
        json_exit(0,'ok',$data);
        
    }
   
    /**
     * 生成mysql管理界面
    **/
    
    public function admin_mysql(){
        error_reporting(0);
        // 将 JSON 返回到 jQuery Ajax 请求的标头
        header('Content-Type: application/json');
        
        // 删除最后一个冒号的函数
        function removeLastChar($string) {
        	$string = substr($string, 0, -1);
        	return $string;
        }
        
        // 创建文件的功能
        function createFile($database, $myFile, $stringData) {
        	$path = public_path() . "/mysql/" . $database . date("Y-m-d_H-i");

        	if (!file_exists($path)) {
        		mkdir($path, 0777,true);
        	}
        	if(!file_exists($path."/includes")){
        	    mkdir($path."/includes", 0777,true);
        	} 
        	
        	$fh = fopen($path . "/" . $myFile . ".php", 'w') or die("无法打开文件");
        	fwrite($fh, $stringData);
        	fclose($fh);
        }
        
        // 函数返回要在侧栏链接中使用的随机字形图标
        function random_glyph_icon() {
        	$glyph_icons = array("asterisk", "plus", "euro", "eur", "minus", "cloud", "envelope", "pencil", "glass", "music", "search", "heart", "star", "star-empty", "user", "film", "th-large", "th", "th-list", "ok", "remove", "zoom-in", "zoom-out", "off", "signal", "cog", "file", "time", "road", "download-alt", "download", "upload", "inbox", "play-circle", "repeat", "refresh", "list-alt", "lock", "flag", "headphones", "volume-off", "volume-down", "volume-up", "qrcode", "barcode", "tag", "tags", "book", "bookmark", "print", "camera", "font", "bold", "italic", "text-height", "text-width", "align-left", "align-center", "align-right", "align-justify", "list", "indent-left", "indent-right", "facetime-video", "picture", "map-marker", "adjust", "tint", "share", "check", "move", "step-backward", "fast-backward", "backward", "play", "pause", "stop", "forward", "fast-forward", "step-forward", "eject", "chevron-left", "chevron-right", "plus-sign", "minus-sign", "remove-sign", "ok-sign", "question-sign", "info-sign", "screenshot", "remove-circle", "ok-circle", "ban-circle", "arrow-left", "arrow-right", "arrow-up", "arrow-down", "share-alt", "resize-full", "resize-small", "exclamation-sign", "gift", "leaf", "fire", "eye-open", "eye-close", "warning-sign", "plane", "calendar", "random", "comment", "magnet", "chevron-up", "chevron-down", "retweet", "shopping-cart", "folder-close", "folder-open", "resize-vertical", "resize-horizontal", "hdd", "bullhorn", "bell", "certificate", "thumbs-up", "thumbs-down", "hand-right", "hand-left", "hand-up", "hand-down", "circle-arrow-right", "circle-arrow-left", "circle-arrow-up", "circle-arrow-down", "globe", "wrench", "tasks", "filter", "briefcase", "fullscreen", "dashboard", "paperclip", "heart-empty", "link", "phone", "pushpin", "usd", "gbp", "sort", "sort-by-alphabet", "sort-by-alphabet-alt", "sort-by-order", "sort-by-order-alt", "sort-by-attributes", "sort-by-attributes-alt", "unchecked", "expand", "collapse-down", "collapse-up", "log-in", "flash", "new-window", "record", "save", "open", "saved", "import", "export", "send", "floppy-disk", "floppy-saved", "floppy-remove", "floppy-save", "floppy-open", "credit-card", "transfer", "cutlery", "header", "compressed", "earphone", "phone-alt", "tower", "stats", "sd-video", "hd-video", "subtitles", "sound-stereo", "sound-dolby", "sound-5-1", "sound-6-1", "sound-7-1", "copyright-mark", "registration-mark", "cloud-download", "cloud-upload", "tree-conifer", "tree-deciduous", "cd", "save-file", "open-file", "level-up", "copy", "paste", "alert", "equalizer", "king", "queen", "pawn", "bishop", "knight", "baby-formula", "tent", "blackboard", "bed", "apple", "erase", "hourglass", "lamp", "duplicate", "piggy-bank", "scissors", "bitcoin", "btc", "xbt", "yen", "jpy", "ruble", "rub", "scale", "ice-lolly", "ice-lolly-tasted", "education", "option-horizontal", "option-vertical", "menu-hamburger", "modal-window", "oil", "grain", "sunglasses", "text-size", "text-color", "text-background", "object-align-top", "object-align-bottom", "object-align-horizontal", "object-align-left", "object-align-vertical", "object-align-right", "triangle-right", "triangle-left", "triangle-bottom", "triangle-top", "console", "superscript", "subscript", "menu-left", "menu-right", "menu-down", "menu-up");
        
        	$rand = array_rand($glyph_icons, 1);
        	return $glyph_icons[$rand];
        }


        if($_POST) {
        
        	$action 	= $_POST["action"];
        	$host 		= $_POST["host"];
        	$username 	= $_POST["username"];
        	$password 	= $_POST["password"];
        
        	// 连接到主机
        	$link = mysqli_connect($host, $username, $password);
        
        	if (!$link) {
        			json_exit(1,'数据库链接失败');
        	}
        
        	// 如果操作是连接，我们请求所有数据库
        	if($action == "connect") {
        		$result = '';
        		$res = mysqli_query($link, "SHOW DATABASES");
        		if (!$res) {
        			//die(json_encode(array('status' => 'error','message'=> '列出数据库失败: ' . mysqli_error($link))));
        			json_exit(1,'数据库错误');
        		}
        		
        		$row_data = [];
        		while ($row = mysqli_fetch_assoc($res)) {
        			$result .= "<option value=\"" .$row['Database'] . "\">" .$row['Database'] . "</option>";
        			array_push($row_data,$row['Database']);
        		}
        
        		if(!$result) {
        			json_exit(1,'数据收集错误',$result);
        		} else {
        			//echo json_encode(array('status' => 'success','result'=> $result));
        			json_exit(0,$row_data,$row_data);
        		}
        
        	}
        
        	// 如果操作是生成管理面板，则流程开始
        	else if ($action == "generate") {
        
        		// 获取数据库名称
        		$database = $_POST["database"];
        		
        		$name = $_POST["name"];
        		
  
        		
        		//$MysqlData = include app_path() . 'ectools_app/' . $name . '/Mysql.php';
        
        		// 收集成功信息并在最后显示给用户
        		$message = "执行日志: <ul>";
        
        		// 选择数据库
        		$db_link = mysqli_select_db($link, $database);
        		if (!$db_link) {
        			die(json_encode(array('status' => 'error','message'=> 'Couldn\'t select database: ' . mysqli_error($link))));
        		}
        
        		// 如果用户表不存在则创建它
        		$sql = "CREATE TABLE IF NOT EXISTS `ect_panel` (`id` int(11) NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `role` int(11) NOT NULL, PRIMARY KEY (`id`) ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2";
        
        		$res = mysqli_query($link, $sql);
        		if (!$res) {
        			die(json_encode(array('status' => 'error','message'=> 'Couldn\'t create users table: ' . mysqli_error($link))));
        		}
        
        		// 插入管理员条目，密码为 MD5'ed
        		mysqli_query($link, "INSERT INTO `ect_panel` (`id`, `name`, `email`, `password`, `role`) VALUES (1, '{$username}', 'admin', '".md5($password)."', 1)");
        
        		// loop to show all the tables and fields
        		$loop = mysqli_query($link, "SHOW tables FROM $database");
        
        		if (!$loop) {
        			die(json_encode(array('status' => 'error','message'=> 'Couldn\'t select table: ' . mysqli_error($link))));
        		}
        

                // 生成过程从这里开始
                // 收集数据库连接信息生成includes/connect.php文件
        		$connection = "<?php
        		\$link = mysqli_connect(\"$host\", \"$username\", \"$password\");
        		mysqli_select_db(\$link, \"$database\");
        		mysqli_query(\$link, \"SET CHARACTER SET utf8\");
        		?>
        		";
        
        		// starting the save.php file which controls create, update, and delete operations on the database.
        		$save = "<?php
        		include(\"includes/connect.php\");
        
        		$"."cat = $"."_POST['cat'];
        		$"."cat_get = $"."_GET['cat'];
        		$"."act = $"."_POST['act'];
        		$"."act_get = $"."_GET['act'];
        		$"."id = $"."_POST['id'];
        		$"."id_get = $"."_GET['id'];
        
        		";
        
        		// collecting the home.php page which shows a full database table of contents
        		$index = "<?php
        		include \"includes/header.php\";
        		?>
        		<table class=\"table table-striped\">
        		<tr>
        		<th class=\"not\">字段列表</th>
        		<th class=\"not\">显示数据</th>
        		</tr>
        		";
        
        		// collecting data for the includes/header.php page which is the header of all pages
        		$header = '<?php
        		error_reporting(0);
        		session_start();
        		if ($_COOKIE["auth"] != "admin_in") {header("location:"."./");}
        			include("includes/connect.php");
        			include("includes/data.php");
        			?>
        			<!DOCTYPE html>
        			<html lang="en">
        			<head>
        				<meta charset="utf-8">
        				<meta http-equiv="X-UA-Compatible" content="IE=edge">
        				<meta name="viewport" content="width=device-width, initial-scale=1">
        				<meta name="author" content="@housamz">
        
        				<meta name="description" content="">
        				<title>' .ucfirst($database). ' Ec tools mysql 面板</title>
        				<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/cosmo/bootstrap.min.css" rel="stylesheet" integrity="sha384-h21C2fcDk/eFsW9sC9h0dhokq5pDinLNklTKoxIZRUn3+hvmgQSffLLQ4G4l2eEr" crossorigin="anonymous">
        
        				<!-- Custom CSS -->
        				<link rel="stylesheet" href="includes/style.css">
        				<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
        
        				<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        				<!-- WARNING: Respond.js doesnt work if you view the page via file:// -->
        				<!--[if lt IE 9]>
        					<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        					<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        				<![endif]-->
        			</head>
        
        			<body>
        
        			<div class="wrapper">
        				<!-- Sidebar Holder -->
        				<nav id="sidebar" class="bg-primary">
        					<div class="sidebar-header">
        						<h3>
        							' .ucfirst($database). ' Ect mysql  面板<br>
        							<i id="sidebarCollapse" class="glyphicon glyphicon-circle-arrow-left"></i>
        						</h3>
        						<strong>
        							' .ucfirst($database). '<br>
        							<i id="sidebarExtend" class="glyphicon glyphicon-circle-arrow-right"></i>
        						</strong>
        					</div><!-- /sidebar-header -->
        
        					<!-- start sidebar -->
        					<ul class="list-unstyled components">
        						<li>
        							<a href="home.php" aria-expanded="false">
        								<i class="glyphicon glyphicon-home"></i>
        								首页
        							</a>
        						</li>
        			';
        
        			// looping all the database tables
        			while($table = mysqli_fetch_array($loop)) {
        
        				// having a name for the table in two cases, all small caps and capitalised
        				$table_name = $table[0];
        				$capital = ucfirst($table_name);
        				$small = strtolower($table_name);
        
        				// collecting the contents for the table main page tableName.php
        				$show = "<?php
        				include \"includes/header.php\";
        				?>
        
        				<a class=\"btn btn-primary\" href=\"edit-".$small.".php?act=add\"> <i class=\"glyphicon glyphicon-plus-sign\"></i> 添加数据 " . $capital . "</a>
        
        				<h1>" . $capital . "</h1>
        				<p>该表包括 <?php echo counting(\"".$table_name."\", \"id\");?> ".$small.".</p>
        
        				<table id=\"sorted\" class=\"table table-striped table-bordered\">
        				<thead>
        				<tr>
        				";
        
        				// collecting data for the edit page
        				$edit = "<?php
        				include \"includes/header.php\";
        				\$data=[];
        
        				$"."act = $"."_GET['act'];
        				if($"."act == \"edit\") {
        					$"."id = $"."_GET['id'];
        					$".$small." = getById(\"".$table_name."\", $"."id);
        				}
        				?>
        
        				<form method=\"post\" action=\"save.php\" enctype='multipart/form-data'>
        					<fieldset>
        						<legend class=\"hidden-first\">添加数据 ".$capital."</legend>
        						<input name=\"cat\" type=\"hidden\" value=\"".$table_name."\">
        						<input name=\"id\" type=\"hidden\" value=\"<?=$"."id?>\">
        						<input name=\"act\" type=\"hidden\" value=\"<?=$"."act?>\">
        				";
        
        				// continue the save page
        				$save .= "
        				if($"."cat == \"".$table_name."\" || $"."cat_get == \"".$table_name."\") {
        					";
        
        				// continue the home page
        				$index .= "
        				<tr>
        					<td><a href=\"" . $small . ".php\">" . $capital . "</a></td>
        					<td><?=counting(\"" . $table_name . "\", \"id\")?></td>
        				</tr>
        				";
        
        				// continue the sidebar in header
        				$icon = random_glyph_icon();
        				$header .= "<li><a href=\"" . $small . ".php\"> <i class=\"glyphicon glyphicon-".$icon."\"></i>" . $capital . " <span class=\"pull-right\"><?=counting(\"" . $table_name . "\", \"id\")?></span></a></li>\n";
        
        				$head='';
        				$body='';
        				$mid='';
        
        				$insert='';
        				$update='';
        				$values='';
        				// finding all the columns in a table
        				$row = mysqli_query($link, "SHOW columns FROM " . $table_name)
        					or die ('cannot select table fields');
        
        				// looping in the columns
        				while ($col = mysqli_fetch_array($row)) {
        					// data for the table in the show page tableName.php
        					$head .= "		\t<th>" . ucfirst(str_replace("_", " ", $col[0])) . "</th>\n";
        					$body .= "	\t<td><?php echo $".$small."s['" . $col[0] . "']?></td>\n";
        
        					if($col[3] != "PRI") {
        						if($col[1] == "text") {
        							$ckeditor = "";
        							if ($_POST["htmlEditor"]) { $ckeditor = "ckeditor "; }
        
        							// continue the edit page with a text area for a type text column
        							$edit .= "
        							<label>" . ucfirst(str_replace("_", " ", $col[0])) . "</label>
        							<textarea class=\"". $ckeditor ."form-control\" name=\"" . $col[0] . "\"><?=$".$small."['" . $col[0] . "']?></textarea><br>
        							";
        						} else {
        							// continue the edit page with an input field
        							$edit .= "
        							<label>" . ucfirst(str_replace("_", " ", $col[0])) . "</label>
        							<input class=\"form-control\" type=\"text\" name=\"" . $col[0] . "\" value=\"<?=$".$small."['" . $col[0] . "']?>\" /><br>
        							";
        						}
        					}
        
        					// check if the column is not the ID to create the corresponding save and insert data
        					if ($col[0] != 'id') {
        
        						$save .= "$" . $col[0] . " = addslashes(htmlentities($"."_POST[\"" . $col[0] . "\"], ENT_QUOTES));\n";
        
        						$insert .= " `" . $col[0] . "` ,";
        
        						if($col[0] == "password") {
        							$attach_password = 1;
        							$values .= " '\".md5($" . $col[0] . ").\"',";
        
        						}else{
        							$attach_password = 0;
        							$values .= " '\".$" . $col[0] . ".\"' ,";
        							$update .= " `" . $col[0] . "` =  '\".$" . $col[0] . ".\"' ,";
        						}
        					}
        
        				} // end row loop
        
        				// continue show page top part
        				$head .= "
        				<th class=\"not\">Edit</th>
        				<th class=\"not\">Delete</th>
        				</tr>
        				</thead>";
        
        				// show page central part
        				$mid = "
        				<?php
        				$".$small." = getAll(\"".$table_name."\");
        				if($".$small.") foreach ($".$small." as $".$small."s):
        					?>
        					<tr>";
        
        				// build the whole page
        				$show .= $head."\n";
        				$show .= $mid."\n";
        				$show .= $body."\n";
        				$show .= "
        						<td><a href=\"edit-".$small.".php?act=edit&id=<?php echo $".$small."s['id']?>\"><i class=\"glyphicon glyphicon-edit\"></i></a></td>
        						<td><a href=\"save.php?act=delete&id=<?php echo $".$small."s['id']?>&cat=".$table_name."\" onclick=\"return navConfirm(this.href);\"><i class=\"glyphicon glyphicon-trash\"></i></a></td>
        						</tr>
        					<?php endforeach; ?>
        					</table>
        					<?php include \"includes/footer.php\";?>
        				";
        
        				$edit .= "<br>
        					<input type=\"submit\" value=\" Save \" class=\"btn btn-success\">
        					</form>
        					<?php include \"includes/footer.php\";?>
        				";
        
        				$save .= "
        
        				if($"."act == \"add\") {
        					mysqli_query(\$link, \"INSERT INTO `".$table_name."` ( ".removeLastChar($insert).") VALUES (".removeLastChar($values).") \");
        				}elseif ($"."act == \"edit\") {
        					mysqli_query(\$link, \"UPDATE `".$table_name."` SET ".removeLastChar($update)." WHERE `id` = '\".$"."id.\"' \"); ";
        
        				if($attach_password == 1) {
        					$save .= "
        					if($"."_POST[\"password\"] && $"."_POST[\"password\"] != \"\") {
        						mysqli_query(\$link, \"UPDATE `".$table_name."` SET  `password` =  '\".md5($"."password).\"' WHERE `id` = '\".$"."id.\"' \");
        					}
        					";
        				}
        
        				$save .= "
        					}elseif ($"."act_get == \"delete\") {
        						mysqli_query(\$link, \"DELETE FROM `".$table_name."` WHERE id = '\".$"."id_get.\"' \");
        					}
        					header(\"location:\".\"".$small.".php\");
        				}
        				";
        
        				// creating the show page tableName.php
        				createFile($database, $small, $show);
        				$message .= "<li>Created page: ".$small.".php</li>";
        
        				// creating the edit page edit-tableName.php
        				createFile($database, "edit-".$small, $edit);
        				$message .= "<li>Created page: edit-".$small.".php</li>";
        
        				// empty all variables
        				$head = "";
        				$body = "";
        
        				$insert = "";
        				$values = "";
        				$update = "";
        
        			} //end table loop
        
        			$save .= "?>";
        
        			$footer ='
        					</div>
        				</div>
        
        				<!-- jQuery Version 1.11.1 -->
        				<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        
        				<!-- Bootstrap Core JavaScript -->
        				<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        
        				<script type="text/javascript" src="//cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
        				<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        				<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
        
        				<script type="text/javascript">
        					$(document).ready(function () {
        						$("#sidebarCollapse, #sidebarExtend").on("click", function () {
        							$("#sidebar").toggleClass("active");
        						});
        
        						$("#sorted").DataTable( {
        							"bStateSave": true,
        							"sPaginationType": "full_numbers"
        						});
        					});
        				</script>
        
        				<script type="text/javascript">
        					function navConfirm(loc) {
        						if (confirm("Are you sure?")) {
        							window.location.href = loc;
        						}
        						return false;
        					}
        				</script>
        			</body>
        			</html>';
        
        			$index .= "</table>
        			<?php include \"includes/footer.php\";?>
        			";
        
        			$header .= "<li><a href=\"logout.php\"><i class=\"glyphicon glyphicon-log-out\"></i>  退出</a></li>
        				</ul>
        
        				<div class=\"visit\">
        					<p class=\"text-center\">鸣谢 MAGE </p>
        					<a href=\"https://gitee.com/pmhw/ectools\" target=\"_blank\" >ECTECT 开源地址</a>
        				</div>
        			</nav><!-- /end sidebar -->
        
        			<!-- Page Content Holder -->
        			<div id=\"content\">";
        
                    createFile($database, "includes/connect", $connection);
                    $message .= "<li>为数据库连接信息创建了 connect.php；</li>";
                    
                    createFile($database, "save", $save);
                    $message .= "<li>为数据库的创建、更新和删除操作创建了 save.php；</li>";
                    
                    createFile($database, "includes/footer", $footer);
                    $message .= "<li>创建 footer.php 来保存页面页脚；</li>";
                    
                    createFile($database, "home", $index);
                    $message .= "<li>创建 home.php 以在起始页有表格；</li>";
                    
                    createFile($database, "includes/header", $header);
                    $message .= "<li>创建 header.php 来保存页面标题；</li>";
                    
                    $library = app_path() . "ectools/Mysql/";
                    $path = public_path() . "mysql/" . $database . date("Y-m-d_H-i");
                    $path1 = "/mysql/" . $database . date("Y-m-d_H-i");
                    
                    copy($library."index.php", $path."/index.php");
                    $message .= "<li>创建 index.php 以获得登录页面；</li>";
                    
                    copy($library."login.php", $path."/login.php");
                    $message .= "<li>创建 login.php 来控制登录；</li>";
                    
                    copy($library."logout.php", $path . "/logout.php");
                    $message .= "<li>创建 logout.php 来控制登录；</li>";
                    
                    copy($library."data.php", $path."/includes/data.php");
                    $message .= "<li>创建 data.php 以准备好所有功能；</li>";
                    
                    copy($library."style.css", $path."/includes/style.css");
                    $message .= "<li>为样式创建了 style.css;</li></ul>";
                    
                    
                  
                    
                    insert_data(app_path() . 'ectools_app/' . $name . '/config.json','"mysql_url":"' . $path1 .'",',3);
                    
                    
                    json_exit(0,'创建成功','<h1>完成的！</h1><h3>默认用户名: ' .$username.' <br> 默认密码: '.$password . '<br><br><a href="'.$path1.'" target="_blank" style="color:blue;">访问管理面板<i class="glyphicon glyphicon-new-window"></i></a></h3><br><br>'.$message);
                    
                    }
                    	
                 
                    
                    } else {
                    	echo json_encode(array('status' => 'error','message'=> 'Unknown error occurred'));
                    }
            }
    
    
    // 查询数据数量-公共接口
    public function EctPlugin(){
        $url = input('post.url');
        $request_code = input('post.request_code');
        $style = input('post.style');
        echo fgc_post_ask($url
            ,array(
                'request_code'=>$request_code,
                'type' => 1,
                'style' => $style
            ));

    }
    
    // 提取数据-公共接口
    public function EctPluginList(){
        $url = input('post.url');
        
        echo fgc_post_ask($url
            ,array(
                'request_code'=>input('post.request_code'),
                'curr' => input('post.curr'),
                'list' => input('post.list'),
                'style' => input('post.style')
            ));
    

    }
    
    // 安装公共接口
    public function Install_(){
        $url = input('post.url');
        $id = input('post.id');
        $style = input('post.style');
        $request_code = input('post.request_code');
        // 返回下载地址
        $data = json_decode(fgc_post_ask($url
        ,array(
            'request_code'=>$request_code,
            'id' => $id,
            'style' => $style
        )),true);
        
         $plugin_download_link = $data['value'];

        
        if($style == 1){
            // 下载插件
            chmod(public_path(),0777);
            // 下载
            $fp_output = fopen(public_path() . '/ectlinshi.zip', 'w');
            $ch = curl_init($plugin_download_link);
            curl_setopt($ch, CURLOPT_FILE, $fp_output);
            curl_exec($ch);
            curl_close($ch);
            
            
            
            $file = public_path() . '/ectlinshi/';
            
            // 解压
            $zip = new \ZipArchive;
            $zip->open(public_path() . '/ectlinshi.zip', \ZipArchive::CREATE);
            $zip->extractTo($file);
            $flag = $zip->close();
            
            $name = getFolderList($file)[0];
            try{
                if(is_dir(dirname($_SERVER['DOCUMENT_ROOT']) . '/extend/' . $name . '/')){
                    deldir($file);
                    unlink(public_path() . '/ectlinshi.zip');
                    chmod(public_path(),0755); 
                    json_exit(1,'该目录已存在：' . dirname($_SERVER['DOCUMENT_ROOT']) . '/extend/' . $name . '/');
                }else{
                    rename($file . $name . '/',dirname($_SERVER['DOCUMENT_ROOT']) . '/extend/' . $name . '/');
                }
            }catch (Exception $e) {
            }
            deldir($file);
            unlink(public_path() . '/ectlinshi.zip');
            chmod(public_path(),0755);
            
            json_exit(0,'下载成功');
        }
        
        if($style == 2){
            // 下载app应用
            chmod(public_path(),0777);
             if(!file_exists(public_path() . '/storage/linshi/')){  // 判断是否存在存放上传文件的目录
                mkdir(public_path() . '/storage/linshi/');  // 建立新的目录
            }
            
            $newFileName = public_path() . '/storage/linshi/ectlinshi.zip';
                        
            $fp_output = fopen($newFileName, 'w');
            $ch = curl_init($plugin_download_link);
            curl_setopt($ch, CURLOPT_FILE, $fp_output);
            curl_exec($ch);
            curl_close($ch);

            

            $zip = new \ZipArchive;
            $zip->open($newFileName, \ZipArchive::CREATE);
            
            // 提取配置文件
            $filesInside = [];
            for ($i = 0; $i < $zip->count(); $i++) {
                if(strpos($zip->getNameIndex($i),'config.json') !== false){
                    array_push($filesInside, $zip->getNameIndex($i));
                }
            }
            
            
            if($filesInside == [] || !$filesInside){
               deldir(public_path() . '/storage/linshi/');
                exit(json_encode(['code'=>1,'msg'=>'程序格式不正确','value'=>$filesInside]));
            }
            
            $strsToArray = strsToArray($filesInside[0],"/");
            
            
            $appname = $strsToArray[0];
            
            $getFolderList = getFolderList(APP_PATH."ectools_app");
            
            
            if(in_array($appname,$getFolderList)){
                deldir(public_path() . '/storage/linshi/');
                exit(json_encode(['code'=>1,'msg'=>'检测到该程序已存在，请重命名你的程序后再下载',]));
            }
   
            $zip->extractTo(public_path() . '/storage/linshi/');
            $zip->close();
            // 移动程序
            rename(public_path() . '/storage/linshi/' . $strsToArray[0] . '/ectools_app/' . $strsToArray[0],APP_PATH.'ectools_app/' . $strsToArray[0]);
            rename(public_path() . '/storage/linshi/' . $strsToArray[0] . '/ectools_view/' . $strsToArray[0],APP_PATH.'ectools_view/' . $strsToArray[0]);
            
            deldir(public_path() . '/storage/linshi/');
            
            exit(json_encode(['code'=>0,'msg'=>'上传成功','value'=>public_path() . '/storage/linshi/' . $strsToArray[0] . '/ectools_app/' . $strsToArray[0]]));
            chmod(public_path(),0755);
            json_exit(0,'下载成功');
        }
    }   
    
    // 读取本地软件 
    
    public function LocalPlugin(){
        $data = getFolderList(dirname($_SERVER['DOCUMENT_ROOT']) . '/extend/');
        $extend_data = [];
       
        foreach ($data as $k){
            $file = dirname($_SERVER['DOCUMENT_ROOT']) . '/extend/' . $k . '/ect.plugin.config.json';
            if(file_exists($file) && is_json($file)){

                    $json = json_decode(file_get_contents($file),true);
                    if(isset($json['plugin_id'])){
                        // dump($json['plugin_id']);
                        array_push($extend_data,$json['plugin_id']);
                    }
            }
        }
        
        return json_encode($extend_data);
    }
    
    // 读取本地软件
    
    public function LocalApp(){
        
        
    }
    
    // 卸载公共接口
    
     public function uninstall_(){
         $style = input('post.style');
         $route = input('post.route');
         if($style == 1){
             if(is_dir($route)){
                 $res = deldir($route);
                 if(!$res){
                     json_exit(1,'卸载失败');
                 }
                 
                 json_exit(0,'卸载成功');
             }
             
             json_exit(0,'应用目录不存在');
         }
         
     }
     
}