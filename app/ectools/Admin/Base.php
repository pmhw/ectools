<?php
namespace app\ectools_app;

// +----------------------------------------------------------------------
// | EC Tools 易开发框架 为快速开发而生 创新、大胆、引领
// +----------------------------------------------------------------------
// | 作者：dpp 
// +----------------------------------------------------------------------
// | 微信：N79823
// +----------------------------------------------------------------------
// | 官网：https://pmhapp.com
// +----------------------------------------------------------------------


/**
 * 后台公共文件
 * 微信：N79823
 * 官网：https://pmhapp.com
**/

use app\BaseController;
use think\facade\View;
use think\facade\Config;
use think\facade\Db;
use ectlog\Log;
use think\facade\Session;
use think\facade\Request;

const TaoBaoIpApi = 'https://ip.taobao.com/outGetIpInfo?ip=';
const TaoBaoIpApi_accessKey = '&accessKey=alibaba-inc';


class Base extends BaseController
{
    public function __construct(){

        //日志写入
        Log::runlog();
        
        $request = request();
        $url = $request->pathinfo();

        // 验证权限
        $admin = Session::get('ect_admin');
        // dump($admin);
        // exit();

        if(!$admin || $admin == null){
            if(trim($url) == "ect_admin"){
                header("Location: ect_login");
            }else{
                header("Location: 404.html");
            }
            exit(); 

        }
        
        // if($admin['type'] > 0){
        //     json_exit(1,'演示站不能修改');
        // }
        
    }

}