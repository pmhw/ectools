<?php
namespace app\ectools_app;

// +----------------------------------------------------------------------
// | EC Tools 易开发框架 为快速开发而生 创新、大胆、引领
// +----------------------------------------------------------------------
// | 作者：dpp 
// +----------------------------------------------------------------------
// | 微信：N79823
// +----------------------------------------------------------------------
// | 官网：https://pmhapp.com
// +----------------------------------------------------------------------

use app\BaseController;
use think\facade\View;
use think\facade\Config;
use think\facade\Db;
use think\facade\Request;
use ectlog\Log;

/**
 * 后台视图控制器
 * 
 * 微信：N79823
 * 官网：https://pmhapp.com
**/

// require_once APP_PATH . 'ectools/Log.php'; 

class ElectronApi extends Base
{  
    
    public function token(){
        // 延时验证 验证通过后才能返回成功
          $token = input('get.token');
          
          if($token != 'evqbK67y6dUBeGsCiqA3s6kmlNAVzI510o7I'){
              exit(json_exit(1,'失败'));
          }
          return json_exit(0,'成功',);
    }
    
    
    public function BaseToken(){
          $token = input('get.token');
          
          if($token != 'evqbK67y6dUBeGsCiqA3s6kmlNAVzI510o7I'){
              exit(json_exit(1,'失败'));
          }        
    }
    
    
    /**
     *TODO:
     *  获取程序列表 applist
     *  
     * $app_fonfig @ app程序数据 
     * $default_controller @ 默认首页
    **/
    public function applist(){
        
      $this->BaseToken();
      
      //获取App程序
      $app_fonfig = EctApp(); 
            
      $default_controller = Config::get('route.default_controller');
      $default_controller = explode('/', $default_controller); 
      
      if(count($default_controller)>1){
          $default_controller = $default_controller[0];
      }else{
         $default_controller = 'index'; 
      }
      
      return json(['app_fonfig'=>$app_fonfig,'default_controller' =>$default_controller]);
      
    }
    
    /**
     *  获取控制器-目录下文件
     *  
     * $file_name 入口文件
     * $files @ array 文件目录
     * 
    **/
    public function appflie($file_name){
        
       $this->BaseToken();
      
       $file = APP_PATH . 'ectools_app/' . $file_name;
       $files = getDirContent($file);
       
       return json(['file'=>$file,'files'=>$files,'file_name'=>$file_name,'type'=>1,]);
    }
    
    
    /**
     *  获取视图层-目录下文件
     *  
     * $file_name 入口文件
     * $files @ array 文件目录
     * 
    **/
    public function appflie_view($file_name){
        
       $this->BaseToken();
      
       $file = APP_PATH . 'ectools_view/' . $file_name;
       $files = getDirContent($file);
       
       return json(['file'=>$file,'files'=>$files,'file_name'=>$file_name,'type'=>1,]);
    }
    
    /**
     * 读取文件内容
    **/
    public function pagecode(){
        
       $this->BaseToken();       
        
        
       $file = input('get.file');
       // 渲染文件名
       $fileee = $file;
       //View::assign('file',$file);
        
       $file_path = input('get.file_path');
       
       // 2022 4 23 优化 组合文件位置
       $file_name = input('get.file_name');
       // 
       $routes =  $file;
       //$file_path = app()->getRootPath();
       // dump(APP_PATH);
       // dump($file_path);
       $route = $file;
       
       if(!empty($file)){
           $file_1 = file_1($file);
           //dump($file);
           $file = $file_path . '/' . $file;
           $file_get_contents = file_get_contents($file);//读取内容
           $route = file_0($route);
           
          // View::assign('route',$route);
           if($file_1 == 'htm' || $file_1 == 'html'){
            $files =  RootPath() . 'app/ectools_view/' . $file_name;
            //View::assign('file_1',$file_1.'l');   
            $file_1 = $file_1.'l';
           }else{
            $files =  RootPath() . 'app/ectools_app/' . $file_name;
            //View::assign('file_1',$file_1);
            $file_1 = $file_1;
           }
           
           // 路径名 + 文件名
           $files = $files . '/' . $routes;
            
            return json(['file'=>$fileee,'route'=>$route,'file_1'=>$file_1,'file_path_save'=>$files,'file_path'=>$files,'file_get_contents'=>trim($file_get_contents)]);
           // 保存路径名
        //   View::assign('file_path_save',$files);
        //   View::assign('file_path',$files);
        //   View::assign('file_get_contents',trim($file_get_contents));
       }else{
           //View::assign('file_path',$file = '');
           return json(['file_path'=>$file]);
       }
    }
    
    /**
     * 删除app
    **/
    public function delete_app(){
        $this->BaseToken(); 
        
        $file_name = input('get.file_name');
        
        if(!$file_name){
            json_exit(1,'文件名为空');
        }
        
        chmod(ECT_APP_PATH,0777);
        if(file_exists(ECT_APP_PATH . $file_name)){
            deldir(ECT_APP_PATH . $file_name);
        }
        chmod(APP_PATH . 'ectools_view',0777);
        if(file_exists(APP_PATH . 'ectools_view/' . $file_name)){
            deldir(APP_PATH . 'ectools_view/' . $file_name);
        }
        chmod(ROUTE_PATH,0777);
        if(file_exists(ROUTE_PATH . $file_name . '.php')){
            unlink(ROUTE_PATH . $file_name . '.php');
        }
        
        chmod(APP_PATH . 'ectools_view',0755);
        chmod(ECT_APP_PATH,0755);
        chmod(ROUTE_PATH,0755);
        
        
        $default_controller = Config::get('route.default_controller');
        $_default_controller = $file_name . '/index';
        
        if($default_controller == $_default_controller){
            reduction_default_controller();
        }
        
        json_exit(0,'删除成功');    
    }
    
    /**
     * 设为入口文件
     * 
     * $file_name 程序文件名目录名
    **/
    public function config_default_controller(){
        $this->BaseToken();
        
        $file_name = input('get.file_name');
        
        if(!$file_name){
           json_exit(1,'请检测参数'); 
        }
        
        $file = config_path() . 'route.php';
        
        $default_controller = Config::get('route.default_controller');
        $_default_controller = $file_name . '/index';

        if($_default_controller == $default_controller){
            reduction_default_controller();
            json_exit(0,'已还原设置'); 
        }else{
            //exit('$default_controller = "' . $default_controller . '";');
            set_chmod($file,'$default_controller = "' . $default_controller . '";','$default_controller = "' . $_default_controller .'";');
            
        }
        
        json_exit(0,'设置成功');        
    }
    
    
    /**
     * 导出程序文件为zip 压缩包
    **/
    public function export_app(){
        
        $this->BaseToken(); 
        
        $php_loaded_extensions = (system_info())['php_loaded_extensions'];
        
        if(!in_array('zip',$php_loaded_extensions)){
            json_exit(1,'请先安装zip扩展');
        }
        
        $name = input('file_name');
        if(!$name){
           json_exit(1,'请检测参数'); 
        }
        
        //判断目录是否存在
        if(!is_dir(public_path() . 'export')){
            mkdir(public_path() . 'export',0755,true);
        }
        
        
        // Using functions
        $filename = $name . '_' . time();//定义导出压缩包名称
        chmod(APP_PATH.'ectools_app/'.$name,0777);
        chmod(APP_PATH.'ectools_view/'.$name,0777);
        chmod(public_path() . 'export',0777);
            $datas = [APP_PATH.'ectools_app/'.$name.'/',APP_PATH.'ectools_view/'.$name.'/'];
            $a = dirToZip($datas,public_path() . 'export/' . $filename .'.zip',$name);
        chmod(APP_PATH.'ectools_app/'.$name,0755);
        chmod(APP_PATH.'ectools_view/'.$name,0755);
        chmod(public_path() . 'export',0755);
        
            
        set_rename($filename . '.zip',$filename . '.ect',public_path() . 'export/');
        
        $url = '/export/' . $filename . '.ect';

        
        if($a['code'] == -200){
           json_exit(0,$a['msg']);  
        }
        
        if($a['code'] == 200 ){
           json_exit(0,'导出成功',$url); 
        }
    }
    
    /**
     * 新建app
    **/
    public function new_app(){
        $this->BaseToken();
        
        $app_filename = input('get.app_filename');
        
        if(!$app_filename){
            json_exit(1,'文件名为空');
        }
        
        if(!(preg_match ("/^[a-z]/i", $app_filename))){
            json_exit(1,'请输入纯字母');
        }
        
        $app_filename = ucfirst(trim($app_filename));
       
        
        $file = APP_PATH . 'ectools_app';
        $file_ = APP_PATH . 'ectools_view';
        
        // 取出数据
        $copy_index = APP_PATH . 'ectools/index';
        $copy_json = APP_PATH . 'ectools/json';
        $copy_route = APP_PATH . 'ectools/route';
        $copy_view = APP_PATH . 'ectools/view';
        $copy_mysql = APP_PATH . 'ectools/mysql';
        
        if(is_dir($file.'/'.$app_filename) || is_dir($file_.'/'.$app_filename)){
           json_exit(1,'入口名已存在'); 
        }
        
        //创建目录
        mkdir ($file.'/'.$app_filename,0777,true);//控制器文件
        mkdir ($file_.'/'.$app_filename,0777,true);//视图层文件
        
        chmod($file,0777);
        chmod($file_,0777);

            //写入index控制器文件
            $data = file_get_contents($copy_index);
            is_file($file.'/'.$app_filename.'/Index.php') OR fclose(fopen($file.'/'.$app_filename.'/Index.php', 'w'));
            
            file_put_contents($file.'/'.$app_filename.'/Index.php',$data);
            set_chmod($file.'/'.$app_filename.'/Index.php','wx',$app_filename);
            //chmod($file.'/'.$app_filename.'/Index.php',0755);
    
            
            //写入json配置文件
            $data = file_get_contents($copy_json);
            is_file($file.'/'.$app_filename.'/config.json') OR fclose(fopen($file.'/'.$app_filename.'/config.json', 'w'));
            file_put_contents($file.'/'.$app_filename.'/config.json',$data);
            chmod($file.'/'.$app_filename.'/config.json',0755);
            
            //写入mysql文件
            $data = file_get_contents($copy_mysql);
            is_file($file.'/'.$app_filename.'/Mysql.php') OR fclose(fopen($file.'/'.$app_filename.'/Mysql.php', 'w'));
            file_put_contents($file.'/'.$app_filename.'/Mysql.php',$data);
            chmod($file.'/'.$app_filename.'/Mysql.php',0755);            
            
            
            //写入视图层
            $data = file_get_contents($copy_view);
            is_file($file_.'/'.$app_filename.'/view.htm') OR fclose(fopen($file_.'/'.$app_filename.'/view.htm', 'w'));
            file_put_contents($file_.'/'.$app_filename.'/view.htm',$data);
            chmod($file_.'/'.$app_filename.'/view.htm',0755);        
        
        chmod($file,0755);
        chmod($file_,0755);
        chmod($file.'/'.$app_filename,0755);
        
        
        //写入路由
        $data = file_get_contents($copy_route);
        chmod(ROUTE_PATH,0777);
        is_file(ROUTE_PATH . $app_filename . '.php') OR fclose(fopen(ROUTE_PATH . $app_filename . '.php', 'w'));
        file_put_contents(ROUTE_PATH . $app_filename . '.php',$data);
        chmod(ROUTE_PATH,0755);
        
        
        json_exit(0,'创建成功');   
    }
    
    
    /**
     * 保存代码
     * 
     * 作者qq32579135
     * 官网：https://pmhapp.com
     * 
     * $data 保存数据
     * $file_path 保存路径
    **/      
    
    public function add_file(){
      $data = input('post.data');  
      $file_path = input('post.file_path');
      $_data = file_get_contents($file_path);
      
      chmod($file_path,0777);
      //
      $insert = file_put_contents($file_path,$data,FILE_USE_INCLUDE_PATH);
      chmod($file_path,0644);
      if(!$insert){
          json_exit(1,'保存失败',$insert);
      }
      json_exit(0,'保存成功',$insert);
      
    }
    
    
     /**
     * 访问接口
     * 
     * 作者qq32579135
     * 官网：https://pmhapp.com
     * 
     * $function_name @方法名
     * $ectool_name @控制器名
     * $directory_name @目录名 
     * $url @route前缀
     * $url2 @route值
     * $data @插入文件内容
    **/       
    public function jump_function(){
        
        $function_name = input('get.function_name');
        $ectool_name = input('get.ectool_name');
        $directory_name = input('get.directory_name');
        
        // return $function_name."\n".$ectool_name."\n".$directory_name;
        
        
        
        //检查文件是否存在 不存在生成
        chmod(ROUTE_PATH,0777);
        is_file(ROUTE_PATH . $directory_name . '.php') OR fclose(fopen(ROUTE_PATH . $directory_name . '.php', 'w'));
        $dome = file_get_contents(ROUTE_PATH . 'dome');
        file_put_contents(ROUTE_PATH . $directory_name . '.php',$dome,FILE_USE_INCLUDE_PATH);
        chmod(ROUTE_PATH,0755);
        
        
        $file = file_get_contents(ROUTE_PATH . $directory_name . '.php');
        
        $url = $directory_name.'/'.$ectool_name.'/'.$function_name;
        
        $url2 = '\app\ectools_app\\' . $directory_name . '\\' . $ectool_name . '@' . $function_name;
        
        $data = $file . "\n" . "Route::rule('" . $url . "', '" . $url2 . "');";
        
        if(strpos($file,$url) > 0 ){
            //包含
            json_exit(0,'已创建',$_SERVER['HTTP_HOST'] . '/' . $url);
        }
        
        chmod(ROUTE_PATH . $directory_name . '.php',0777);
        $insert = file_put_contents(ROUTE_PATH . $directory_name . '.php',$data,FILE_USE_INCLUDE_PATH);
        chmod(ROUTE_PATH . $directory_name . '.php',0755);
        if($insert){
            json_exit(0,'创建成功',$_SERVER['HTTP_HOST'] . '/' . $url);
        }
    }
    
   /**
     * 新建文件接口
     * 
     * 作者qq32579135
     * 官网：https://pmhapp.com
     * 
     * $file_name @文件名
     * $file_names @目录名
     * $type @ 属性 1 控制器 2视图层
    **/    
    public function new_file(){
        $file_name = input('get.file_name');
        if(!$file_name){
            json_exit(1,'文件名为空');
        }        
        $file_names = input('get.file_names');
        if(!$file_names){
            json_exit(1,'路径名为空');
        }
        
        $type = input('get.type');
        if(!$type){
            json_exit(1,'type为空');
        }
        // 
        if($type == 1){
            
            $file =  RootPath() . 'app/ectools_app/' . $file_names;
            chmod($file,0777);
        }else{
            // 取后缀
            $_file_name = file_1($file_name);
            if($_file_name != 'htm'){
               json_exit(1,'视图层，后缀必须为.htm'); 
            }
            
            $file =  RootPath() . 'app/ectools_view/' . $file_names;  
            chmod($file,0777);
        } 
        
        $a = fopen($file . '/' . $file_name, "w");
        chmod($file,0755);
        
        if($a){
            json_exit(0,'创建成功');
        }
        
    }
    
    
    /**
     * 删除文件接口
     * 
     * 作者 微信：N79823
     * 官网：https://pmhapp.com
     * 
     * $file_path @文件路径
     * $file_names @目录名
     * $type @ 属性 1 控制器 2视图层
    **/     
    public function delete_file(){
        $this->BaseToken();
        
        
        $file_path = input('get.file_path');
        
        
        if (file_exists($file_path)) {
          //文件存在
          chmod($file_path,0777);
            if (unlink($file_path)) {
              //删除成功
              json_exit(0,'删除成功');
            } else {
              //删除失败
              json_exit(1,'删除失败');
            }
          chmod($file_path,0755);
        } else {
          //文件不存在
          json_exit(1,'文件不存在');
        }
    }
 
}