<?php
namespace app\ectools_app;

// +----------------------------------------------------------------------
// | EC Tools 易开发框架 为快速开发而生 创新、大胆、引领
// +----------------------------------------------------------------------
// | 作者：dpp 
// +----------------------------------------------------------------------
// | 微信：N79823
// +----------------------------------------------------------------------
// | 官网：https://pmhapp.com
// +----------------------------------------------------------------------

use think\captcha\facade\Captcha;

/**
 * 验证码
 * 微信：N79823
 * 官网：https://pmhapp.com
 * 
 * 访问验证码 您的域名/Vcode
 * 
 * 首先使用Composer安装think-captcha扩展包：
 * composer require topthink/think-captcha
 * 
 * 界面内渲染 <img src="{{:url('Vcode/index')}}" alt="captcha"/>
**/

class Vcode{
    
    public function index(){
        
        return Captcha::create();
        
    }
}