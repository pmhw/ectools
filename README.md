# ectools


#### 简介

ECT (Ec tools) 这是一款现代化、快速、高效、便捷、灵活、方便扩展的应用开发骨架。

 **🚀 程序开源地址

gitee:https://gitee.com/pmhw/ectools

演示站点：http://e.kaiyuantong.cn/ect_login** 


ECT 诞生于 2022年 作者仅花费1个月时间开发上线 web 1.0 和 内测桌面端应用 😊

15年接触 php 软件开发，熟悉 python、c++、js 等语言

为了寻找快速开发解决方案 尝试各种框架 Yii Laravel 以及Tinkphp等 各有千秋

与19年开始诞生做自己的后端快速开发框架，目的是为了减少软件开发周期 以及 代码沉淀等



#### 特色

🐬 适合前端程序员、服务端小白，快速上手写服务端程序、大型项目。

🚀 超强的可扩展性，应用化，模块化，插件化机制构造极致的开发效率。

🛰 高效批量且自动生成Api接口，内置调试器第一时间展示错误，一站式解决（程序接口，web应用开发，插件开发）。

🚨 助手面板、行为日志分析、一键启调试模式、导出 ect 格式文件，无忧迁移WebApp。

🌝 2.0 优化控制器目录 ectools 内 方便用户二次开发。

💞 多应用模式，一键创建app，每个应用隔离，可作为接口，可做web网页，可做插件等。

💦 ectools plugin(插件库) ectools app(应用库) ectools view(视窗) 。

➕对接 七牛云 微信支付 支付宝支付 微信小程序接口 微信公众号接口 等 更多插件适配中 让用户开箱即用。

🛑 全面监控系统报错，报错日志写入数据库，方便定位错误信息。

💖 基于Tp6，快速、简单的面向对象的轻量级PHP开发框架 敏捷WEB应用开发和简化企业应用开发而诞生的。

🈴 采用PHP7强类型（严格模式）、支持更多的PSR规范多、应用支持ORM组件、独立改进的中间件机制更强大和易用的查询全新的事件系统、支持容器invoke回调模板、引擎组件独立内部功能、中间件化SESSION机制改进缓存及日志支持多通道、引入Filesystem组件、对Swoole以及协程支持改进对IDE更加友好、统一和精简大量用法。

🌐 快速生成 Mysql panel ,一键生成 数据库表管理入口文件。




#### 安装教程

!> 支持宝塔面板一键部署


1.导入源代码到根目录

2.设置伪静态



```
location / {
    if (!-e $request_filename){
        rewrite  ^(.*)$  /index.php?s=$1  last;   break;
    }
}
    
```


3.运行目录设为 public

4.导入数据库文件

5.配置数据库信息 config/database.php



```
<?php
/**
 * 主数据库配置中心
 * 
**/
$datasss = [
    
    'mysql' => [
        // 数据库类型
        'type'            => env('database.type', 'mysql'),
        // 服务器地址
        'hostname'        => env('database.hostname', '127.0.0.1'),
        // 数据库名
        'database'        => env('database.database', ''),//修改为自己的数据库名
        // 用户名
        'username'        => env('database.username', ''),//修改为自己的用户名
        // 密码
        'password'        => env('database.password', ''),//修改为自己的密码
        // 端口
        'hostport'        => env('database.hostport', '3306'),
        // 数据库连接参数
        'params'          => [],
        // 数据库编码默认采用utf8
        'charset'         => env('database.charset', 'utf8'),
        // 数据库表前缀
        'prefix'          => env('database.prefix', 'ect_'),

        // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
        'deploy'          => 0,
        // 数据库读写是否分离 主从式有效
        'rw_separate'     => false,
        // 读写分离后 主服务器数量
        'master_num'      => 1,
        // 指定从服务器序号
        'slave_no'        => '',
        // 是否严格检查字段是否存在
        'fields_strict'   => true,
        // 是否需要断线重连
        'break_reconnect' => false,
        // 监听SQL
        'trigger_sql'     => env('app_debug', true),
        // 开启字段缓存
        'fields_cache'    => false,
    ]
    // 更多的数据库配置信息
];
```

   

#### 参与贡献

dpp 




先行版 1.0.2 未发布
1.修复新建文件刷新问题

先行版 1.0.1 已发布
1.增加上传函数 upload_file()

2.增加 arraySort() 函数 二维数组根据某个字段排序

3.增加 getDistance() 函数 经纬度计算距离

4.增加 upload_file() 函数 文件上传

5.增加 laytable() 函数 layui返回格式封装

6.增加 ret_info() 函数 返回格式封装

extend 插件内增加 编写高德SDK 可自行选择删除

增加应用 menu.json 文件 可自行编写后台路径

修复打开调试模式无法显示错误 报500 问题

10.修复视图层开发时 按键重复输入问题

11.修复menu.json配置错误时导致菜单栏接口失效问题

先行版 1.0.0 已发布
初代版本发布